grammar FSE;

/* 
install https://marketplace.visualstudio.com/items?itemName=mike-lischke.vscode-antlr4 
Windows VS code ANTLR icon spinning forever? -> install the current Java JRE and add C:\Program Files\Java\jdk-14.0.2\bin to the PATH
Ubuntu: VS code ANTLR icon spinning forever? -> sudo apt install antlr4

then use this VSCode settings:

"antlr4.generation": {
    "mode": "external",
    "language": "Go",
    "listeners": true,
    "visitors": false,
    "outputDir": "parser"
},

if Ubuntu won't show diagnostics of antlr4, try
antlr47 -o parser -Dlanguage=Go CasConfig.g4
where antlr47 is a copy of antlr4 with CLASSPATH pointing to
the newest runtime:

    #!/bin/sh

    CLASSPATH=/home/lercher/.vscode/extensions/mike-lischke.vscode-antlr4-2.0.3/antlr/antlr4-4.7.2-SNAPSHOT-complete.jar:/usr/share/java/stringtemplate4.jar:/usr/share/java/antlr4.jar:/usr/share/java/antlr4-runtime.jar:/usr/share/java/antlr3-runtime.jar/:/usr/share/java/treelayout.jar
    exec java -cp $CLASSPATH org.antlr.v4.Tool "$@"

*/


// --------------------------------------------------------- lexer hidden elements

COMMENT: '/*' .*? '*/' -> channel(HIDDEN);

LINE_COMMENT: ('//' | '#') ~[\r\n]* -> channel(HIDDEN);

WS: [ \t\r\n] -> channel(HIDDEN);

// --------------------------------------------------------- lexer normal elements

// single inner _/- are allowed, inner digits are allowed and everything if its enclosed in brackets
fragment IDENTIFIER: [A-Za-z] (('_' | '-')? [0-9A-Za-z])* | '[' .*? ']'; 

// a name is just an identifier
NAME : IDENTIFIER;

// $var
VAR : '$' IDENTIFIER;

// "...\"..."
TEXT : '"' (~[\r\n"] | '\\"')*  '"';

// Duration is a number plus s,m,h
fragment NUMBER : [1-9][0-9]* ('.' [0-9]+)?;

DURATION: NUMBER ('s' | 'm' | 'h');

// --- text line 1\nline 2 ... ---
DESCRIPTION : '---' .*? WS '---' WS;

// handle characters which failed to match any other token, must be last
ERRORCHARACTER : . ;


// --------------------------------------------------------- parser

main: 
    header
    state+ 
EOF;

header:
    'workflow' named
;

named:
    name=NAME
    (':' option=TEXT)?
    ('as' display=TEXT)?
    ('for' lnames=TEXT (','? rnames=TEXT)*)?
    (description=DESCRIPTION)?
;

// --------------------------------------------------------- states
state:
    'state' named
    '{'
        initialize?
        after?
        event*
        state* // inner states
    '}'
;

initialize:
    'initialize'
    consequences
;

after:
    'after' duration=DURATION
    consequences
;

event:
    'event' named
    consequences
;

// --------------------------------------------------------- consequences

consequences:
    cs=consequence*
    ';'
;

consequence:
    ( assign
    | funcall
    | ifgoto
    | terminate
    )
;

assign:
    lvar=VAR '=' ( param | call )
;

funcall:
    call
;

call:
    name=NAME '(' param* ')'
;

param: 
    ( rtext=TEXT 
    | rvar=VAR
    )
;

ifgoto:
    (
        'if' lvar=VAR 
        (
            comp=('==' | '!=') 
            param
        )?
    )?
    ( 'goto' gototarget=( VAR | NAME )
    | 'halt'
    )
;

terminate:
    'terminate'
;
