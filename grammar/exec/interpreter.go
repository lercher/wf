package exec

import (
	"fmt"
	"time"

	"gitlab.com/lercher/wf/grammar/walker"

	"go.temporal.io/sdk/log"
	"go.temporal.io/sdk/workflow"
)

// Actions is a dispatcher for handling funcalls by name defined as a consequence in an FSE.
type Actions func(in *Interpreter, name string, parameters []string) workflow.Future

// Interpreter is an execution environment for an FSE for termporal.io
type Interpreter struct {
	WF                *walker.Workflow
	Ctx               workflow.Context
	Actions           Actions
	Environment       map[string]string
	ActivityOptions   workflow.ActivityOptions
	CurrentState      *walker.State
	idle              bool
	Timer             workflow.Future
	TimerConsequences []walker.Consequence
	TimerDeadline     time.Time
	logger            log.Logger
}

// New creates a new interpreter with an environment for variables for an FSE.
// If actions is nil, all funcalls are dispatched to tempoaral.io actions
// that must be either nil->string, string->string or []string->string according
// to the parameter list's length of 0, 1 or more accordingly.
func New(ctx workflow.Context, wf *walker.Workflow, actions Actions) *Interpreter {
	if actions == nil {
		actions = ActionDispatcher
	}
	return &Interpreter{
		WF:          wf,
		Ctx:         ctx,
		Actions:     actions,
		Environment: make(map[string]string),
		ActivityOptions: workflow.ActivityOptions{
			ScheduleToStartTimeout: time.Hour * 8,
			StartToCloseTimeout:    time.Hour * 24,
		},
		logger: workflow.GetLogger(ctx),
	}
}

func (in *Interpreter) describeState() (WFState, error) {
	return Describe(in, true, nil)
}

func (in *Interpreter) describeStateFor(roles []string) (WFState, error) {
	return Describe(in, false, roles)
}

// Run evaluates an FSE within a temporal.io workflow function.
// Typical usage is: parse an FSE source, create an Interpreter with an Actions dispatcher func
func (in *Interpreter) Run() error {
	if in == nil || in.WF == nil || len(in.WF.States) == 0 {
		// without a WF nor top level states, we are finished immediately
		return nil
	}

	// attach queries:
	workflow.SetQueryHandler(in.Ctx, QueryWorkflowWFState, in.describeState)
	workflow.SetQueryHandler(in.Ctx, QueryWorkflowWFStateFor, in.describeStateFor)

	return in.eventLoop(in.WF.States[0].Name)
}

func (in *Interpreter) eventLoop(initial string) error {
	var err error
	detectLoopCount := 0
	res := result{gotoState: initial} // initialize the initial state
	for {
		switch {
		case res.terminate:
			in.idle = true
			return nil // Workflow done successfully
		case res.gotoState != "":
			// TODO: detect and error-out goto loops here
			detectLoopCount++
			newstate := res.gotoState
			if s, ok := in.WF.AllStates[newstate]; ok {
				res, err = in.gotostate(s)
				if err != nil {
					return fmt.Errorf("initializing %q: %v", newstate, err)
				}
				if detectLoopCount > 20 {
					return fmt.Errorf("initializing %q: state-hop count is too large (%v), check the goto consequences", newstate, detectLoopCount)
				}
				continue
			}
		}
		detectLoopCount = 0 // not a tight loop with goto state consequences

		// wf idles, it should wait for a Timer or for an event's trigger, therefor
		// compose a selector waiting for "trigger" or the timer firing
		var evt string
		var consequences []walker.Consequence
		s := workflow.NewSelector(in.Ctx)
		triggerChannel := workflow.GetSignalChannel(in.Ctx, SignalWorkflowSignalEvent)
		s.AddReceive(triggerChannel, func(c workflow.ReceiveChannel, more bool) {
			c.Receive(in.Ctx, &evt)
			if tr, ok := in.trigger(evt); ok {
				in.logger.Info("triggered", "event", evt)
				consequences = tr.Consequences
			} else {
				in.logger.Warn("received non-active trigger", "event", evt)
			}
		})
		if in.Timer != nil {
			s.AddFuture(in.Timer, func(f workflow.Future) {
				evt = "*timer*"
				in.logger.Info("timer")
				consequences = in.TimerConsequences
				in.Timer = nil
				in.TimerConsequences = nil
				in.TimerDeadline = time.Time{}
			})
		}
		in.idle = true
		s.Select(in.Ctx)
		in.idle = false
		res, err = in.doAll(consequences)
		if err != nil {
			return fmt.Errorf("event %q: %v", evt, err)
		}
		// continue for-ever, hand over the new res to start of for loop
	}
}

// currentTriggers adds all events of the current state and all its parent states to a list
func (in *Interpreter) currentTriggers() (list []*walker.Event) {
	for s := in.CurrentState; s != nil; s = s.Parent {
		for i := range s.Events {
			switch s.Events[i].Type {
			case "event":
				list = append(list, &s.Events[i])
			}
		}
	}
	return list
}

// trigger finds a named event within currentTriggers
func (in *Interpreter) trigger(name string) (*walker.Event, bool) {
	triggers := in.currentTriggers()
	for i := range triggers {
		if triggers[i].Name == name {
			return triggers[i], true
		}
	}
	return nil, false
}

func (in *Interpreter) gotostate(s *walker.State) (result, error) {
	var res result
	if in.CurrentState == s {
		return res, nil
	}

	var err error

	leaving := "*new*"
	if in.CurrentState != nil {
		leaving = in.CurrentState.Name
	}
	in.CurrentState = s

	in.logger.Info("initializing",
		"state", s.Name,
		"leaving", leaving,
	)
	in.Timer = nil
	in.TimerConsequences = nil
	in.TimerDeadline = time.Time{}
	for i := range s.Events {
		switch s.Events[i].Type {
		case "initialize":
			res, err = in.doAll(s.Events[i].Consequences)
			if err != nil {
				return res, fmt.Errorf("%s of %s: %v", s.Events[i].Type, s.Name, err)
			}
		case "after":
			after, err := time.ParseDuration(s.Events[i].Parameter)
			if err != nil {
				return res, fmt.Errorf("%s of %s: %v", s.Events[i].Type, s.Name, err)
			}
			in.logger.Info("after", "delay", after)
			in.Timer = workflow.NewTimer(in.Ctx, after)
			in.TimerConsequences = s.Events[i].Consequences
			in.TimerDeadline = workflow.Now(in.Ctx).Add(after)
		}
	}
	return res, nil
}

func (in *Interpreter) doAll(cs []walker.Consequence) (result, error) {
	for i := range cs {
		res, err := in.doOne(cs[i])
		switch {
		case err != nil:
			return res, fmt.Errorf("action #%d: %v", i, err)
		case res.halt:
			res.halt = false
			break
		case res.escalate():
			return res, nil
		}
	}
	return result{}, nil
}

func (in *Interpreter) doOne(c walker.Consequence) (res result, err error) {
	in.logger.Info(c.Action, "line", c.Line)
	switch c.Action {
	default:
		return res, fmt.Errorf("unknown action type %q", c.Action)

	case "assign":
		err = in.eval(c, func(f workflow.Future) error {
			var s string
			err := f.Get(in.Ctx, &s)
			if err != nil {
				return err
			}
			in.Environment[c.LVar] = s
			return nil
		})
	case "funcall":
		err = in.eval(c, func(f workflow.Future) error {
			return f.Get(in.Ctx, nil)
		})

	case "if-goto", "goto":
		err = in.doIf(c, func() {
			switch {
			case c.Goto == "":
				res.halt = true
			case c.Goto[0] == '$':
				res.gotoState = in.Environment[c.Goto]
			default:
				res.gotoState = c.Goto
			}
		})

	case "if-halt", "halt":
		err = in.doIf(c, func() {
			res.halt = true
		})

	case "terminate":
		res.terminate = true
	}
	return res, err
}

func (in *Interpreter) doIf(c walker.Consequence, iftrue func()) error {
	if c.LVar == "" {
		// unconditional goto/halt
		iftrue()
		return nil
	}

	lvalue := in.Environment[c.LVar]
	evaluatesToTrue := false
	switch c.Operator {
	case "":
		// if $var goto/halt
		evaluatesToTrue = (lvalue != "")
	case "==":
		// if $var == x goto/halt
		if len(c.Parameters) != 0 {
			return fmt.Errorf("illegal number of parameters for %q: %d", c.Operator, len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		evaluatesToTrue = (lvalue == rvalue)
	case "!=":
		// if $var != x goto/halt
		if len(c.Parameters) != 0 {
			return fmt.Errorf("illegal number of parameters for %q: %d", c.Operator, len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		evaluatesToTrue = (lvalue != rvalue)
	default:
		return fmt.Errorf("unsupported comparison op %q", c.Operator)
	}

	if evaluatesToTrue {
		// if condition evaluated to true
		iftrue()
	}
	return nil
}

func (in *Interpreter) rvalue(literal, variable string) string {
	if variable != "" {
		return in.Environment[variable]
	}
	return literal
}

func (in *Interpreter) rvalues(ps []walker.Parameter) (list []string) {
	list = make([]string, len(ps))
	for i := range ps {
		list[i] = in.rvalue(ps[i].Text, ps[i].Var)
	}
	return list
}

func (in *Interpreter) eval(c walker.Consequence, result func(f workflow.Future) error) error {
	switch c.Function {
	case "":
		if len(c.Parameters) != 1 {
			return fmt.Errorf("illegal number of parameters for assignment: want 1, got %d", len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		return result(Resolved(rvalue))
	default:
		f := in.Actions(in, c.Function, in.rvalues(c.Parameters))
		return result(f)
	}
}
