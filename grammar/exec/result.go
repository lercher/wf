package exec

type result struct {
	terminate bool
	halt      bool
	gotoState string
}

func (r result) escalate() bool {
	return r.terminate || r.gotoState != ""
}
