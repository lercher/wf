// Package exec implements an interpreter of the FSE grammar on top of temporal.io
package exec

const (
	// QueryWorkflowWFState is the query to retreive an fse interpreter's complete state's name. The query has no parameters.
	QueryWorkflowWFState = "wfstate"
	// QueryWorkflowWFStateFor is the query to retreive an fse interpreter's state for particular roles's name. The query has a single []string argument, the roles.
	QueryWorkflowWFStateFor = "wfstate-for"

	// SignalWorkflowSignalEvent signals a workflow that an external event had happened. Has one parameter of type string, the event's name.
	// Available events can be retrieved via QueryWorkflowWFState or QueryWorkflowWFStateForFor in the Triggers result.
	SignalWorkflowSignalEvent = "signal-event"
)
