package exec

import (
	"go.temporal.io/sdk/workflow"
)

// ActionDispatcher dispatches all named calls to workflow.ExecuteActivity
func ActionDispatcher(in *Interpreter, name string, parameters []string) workflow.Future {
	ctx := workflow.WithActivityOptions(in.Ctx, in.ActivityOptions)
	logger := workflow.GetLogger(ctx)
	logger.Info("funcall", "name", name, "parameters", parameters)

	pars := make([]interface{}, len(parameters))
	for i := range parameters {
		pars[i] = parameters[i]
	}
	return workflow.ExecuteActivity(ctx, name, pars...)
}
