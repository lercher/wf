package exec

import (
	"time"

	"gitlab.com/lercher/wf/grammar/walker"
	"go.temporal.io/sdk/workflow"
)

// WFState describes the current fse workflow instance
type WFState struct {
	walker.Named
	Idle          bool              `json:"idle,omitempty"`
	TaskQueue     string            `json:"taskqueue,omitempty"`
	ID            string            `json:"id,omitempty"`
	RunID         string            `json:"runid,omitempty"`
	CurrentState  walker.Named      `json:"currentState,omitempty"`
	Triggers      []walker.Named    `json:"triggers,omitempty"`
	TimerDeadline time.Time         `json:"timerDeadline,omitempty"`
	Environment   map[string]string `json:"environment,omitempty"`
}

// Describe describes the workflow instance in its current state.
// If allTriggers is true, all current triggers are returned and roles is ignored.
// If allTriggers is false only those current triggers are returned that have matching roles in their "for" list.
func Describe(in *Interpreter, allTriggers bool, roles []string) (WFState, error) {
	logger := workflow.GetLogger(in.Ctx)

	info := workflow.GetInfo(in.Ctx)
	fse := WFState{
		Named:         in.WF.Named,
		ID:            info.WorkflowExecution.ID,
		RunID:         info.WorkflowExecution.RunID,
		TaskQueue:     info.TaskQueueName,
		CurrentState:  walker.Named{},
		Triggers:      nil,
		TimerDeadline: in.TimerDeadline,
		Environment:   in.Environment,
		Idle:          in.idle,
	}

	triggers := in.currentTriggers()
	for i := range triggers {
		if allTriggers || triggers[i].IntersectsWith(roles) {
			fse.Triggers = append(fse.Triggers, triggers[i].Named)
		}
	}

	if in.CurrentState != nil {
		fse.CurrentState = in.CurrentState.Named
	}

	logger.Info("describe called", "allTriggers", allTriggers, "roles", roles, "fse", fse)
	return fse, nil
}
