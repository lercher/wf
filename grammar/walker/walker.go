package walker

import (
	"fmt"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/lercher/wf/grammar/parser"
)

// wlk can be used to walk an antlr4 parse tree
type wlk struct {
	*parser.BaseFSEListener
	*ErrorListener
	tokenstream  antlr.TokenStream
	wf           Workflow
	stateStack   []*State
	consequences []Consequence
	funcall      Consequence
}

// newWlk creates a new parse tree walker
func newWlk(el *ErrorListener, ts antlr.TokenStream) *wlk {
	w := new(wlk)
	w.tokenstream = ts
	w.ErrorListener = el
	w.wf.AllStates = make(map[string]*State)
	return w
}

// func (w *Walker) EnterEveryRule(ctx antlr.ParserRuleContext) {
// Don't duplicate this method as a template, use
// func (w *Walker) EnterX(ctx *parser.XContext) {
// }

func stringFromToken(t antlr.Token) string {
	if t == nil {
		return ""
	}
	s := t.GetText()
	if len(s) >= 2 {
		s = s[1 : len(s)-1] // remove ""
	}
	s = strings.ReplaceAll(s, `\"`, `"`)
	return s
}

func textFromToken(t antlr.Token, pre, post string, detab bool) string {
	if t == nil {
		return ""
	}
	tt := t.GetText()
	tt = strings.TrimSpace(tt)
	tt = strings.TrimPrefix(tt, pre)
	tt = strings.TrimSuffix(tt, post)
	if detab {
		tt = detabText(tt)
	}
	tt = strings.TrimSpace(tt)
	return tt
}

func (w *wlk) rolenames(ctx parser.INamedContext, firstrolenametoken, lastrolenametoken antlr.Token) (list []string) {
	if firstrolenametoken == nil {
		return nil
	}
	if lastrolenametoken == nil {
		lastrolenametoken = firstrolenametoken
	}

	for i := firstrolenametoken.GetTokenIndex(); i <= lastrolenametoken.GetTokenIndex(); i++ {
		t := w.tokenstream.Get(i)
		if t.GetTokenType() == parser.FSELexerTEXT {
			tt := stringFromToken(t)
			list = append(list, tt)
		}
	}
	return list
}

func (w *wlk) makeNamed(ctx parser.INamedContext) Named {
	n := textFromToken(ctx.GetName(), "[", "]", false)
	o := stringFromToken(ctx.GetOption())
	d := textFromToken(ctx.GetDescription(), "---", "---", true)
	di := stringFromToken(ctx.GetDisplay())
	list := w.rolenames(ctx, ctx.GetLnames(), ctx.GetRnames())

	return Named{
		Name:        n,
		Option:      o,
		Display:     di,
		Description: d,
		Roles:       list,
	}
}

func (w *wlk) ExitHeader(ctx *parser.HeaderContext) {
	w.wf.Named = w.makeNamed(ctx.Named())
}

func (w *wlk) EnterState(ctx *parser.StateContext) {
	w.stateStack = append(w.stateStack, &State{}) // push
}

func (w *wlk) ExitState(ctx *parser.StateContext) {
	s := w.state()
	s.Line = ctx.GetStart().GetLine()
	s.Named = w.makeNamed(ctx.Named())
	w.stateStack = w.stateStack[:len(w.stateStack)-1] // pop
	switch {
	default:
		w.wf.States = append(w.wf.States, *s)
	case len(w.stateStack) > 0:
		s.Parent = w.state()
		s.Parent.States = append(s.Parent.States, *s)
	}
	if _, ok := w.wf.AllStates[s.Name]; ok {
		w.SemErr(ctx.Named().GetName(), fmt.Sprintf("duplicate state name %q", s.Name))
	}
	w.wf.AllStates[s.Name] = s
}

func (w *wlk) state() *State {
	return w.stateStack[len(w.stateStack)-1]
}

func (w *wlk) EnterInitialize(ctx *parser.InitializeContext) {
	w.consequences = nil
}

func (w *wlk) ExitInitialize(ctx *parser.InitializeContext) {
	s := w.state()
	s.Events = append(s.Events, Event{
		Type:         "initialize",
		Consequences: w.consequences,
	})
}

func (w *wlk) EnterAfter(ctx *parser.AfterContext) {
	w.consequences = nil
}

func (w *wlk) ExitAfter(ctx *parser.AfterContext) {
	s := w.state()
	s.Events = append(s.Events, Event{
		Type:         "after",
		Parameter:    textFromToken(ctx.GetDuration(), "", "", false),
		Consequences: w.consequences,
	})
}

func (w *wlk) EnterEvent(ctx *parser.EventContext) {
	w.consequences = nil
}

func (w *wlk) ExitEvent(ctx *parser.EventContext) {
	s := w.state()
	s.Events = append(s.Events, Event{
		Type:         "event",
		Named:        w.makeNamed(ctx.Named()),
		Consequences: w.consequences,
	})
}

func (w *wlk) EnterTerminate(ctx *parser.TerminateContext) {
	c := Consequence{
		Action: ctx.GetStart().GetText(),
		Line:   ctx.GetStart().GetLine(),
	}
	w.consequences = append(w.consequences, c)
}

func (w *wlk) ExitIfgoto(ctx *parser.IfgotoContext) {
	c := Consequence{
		Action:     ctx.GetStart().GetText(),
		Line:       ctx.GetStart().GetLine(),
		Goto:       textFromToken(ctx.GetGototarget(), "", "", false),
		Operator:   textFromToken(ctx.GetComp(), "", "", false),
		LVar:       textFromToken(ctx.GetLvar(), "", "", false),
		Parameters: w.funcall.Parameters,
	}
	if c.Action == "if" {
		switch c.Goto {
		case "":
			c.Action = "if-halt"
		default:
			c.Action = "if-goto"
		}
	}
	w.consequences = append(w.consequences, c)
	w.funcall = Consequence{}
}

func (w *wlk) ExitFuncall(ctx *parser.FuncallContext) {
	c := Consequence{
		Action:     "funcall",
		Line:       ctx.GetStart().GetLine(),
		Function:   w.funcall.Function,
		Parameters: w.funcall.Parameters,
	}
	w.consequences = append(w.consequences, c)
	w.funcall = Consequence{}
}

func (w *wlk) ExitAssign(ctx *parser.AssignContext) {
	c := Consequence{
		Action:     "assign",
		Line:       ctx.GetStart().GetLine(),
		LVar:       textFromToken(ctx.GetLvar(), "", "", false),
		Function:   w.funcall.Function,
		Parameters: w.funcall.Parameters,
	}
	w.consequences = append(w.consequences, c)
	w.funcall = Consequence{}
}

func (w *wlk) EnterCall(ctx *parser.CallContext) {
	w.funcall.Function = textFromToken(ctx.GetName(), "", "", false)
}

func (w *wlk) EnterParam(ctx *parser.ParamContext) {
	p := Parameter{
		Var:  textFromToken(ctx.GetRvar(), "", "", false),
		Text: stringFromToken(ctx.GetRtext()),
	}
	w.funcall.Parameters = append(w.funcall.Parameters, p)
}
