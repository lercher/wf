package walker

import (
	"fmt"

	"github.com/antlr4-go/antlr/v4"
)

// ErrorListener records and logs syntax errors produced by the antlr4 parser
type ErrorListener struct {
	*antlr.DiagnosticErrorListener
	Diagnostics []Diagnostic
}

// Diagnostic represents a locatable error in an FSE spec
type Diagnostic struct {
	Type    string
	Line    int
	Column  int
	Message string
}

func (d Diagnostic) String() string {
	return fmt.Sprintf("%s error line %d:%d %s", d.Type, d.Line, d.Column, d.Message)
}

// ErrorCount counts the number of Diagnostic messages
func (el *ErrorListener) ErrorCount() int {
	return len(el.Diagnostics)
}

// Error makes ErrorListener itself an error
func (el *ErrorListener) Error() string {
	return fmt.Sprintf("%d error(s) detected %v", el.ErrorCount(), el.Diagnostics)
}

// SyntaxError implements an interface method of antlr.DiagnosticErrorListener
func (el *ErrorListener) SyntaxError(
	recognizer antlr.Recognizer,
	offendingSymbol interface{},
	line, column int,
	msg string,
	e antlr.RecognitionException,
) {
	el.Diagnostics = append(el.Diagnostics, Diagnostic{"syntax", line, column, msg})
}

// Semantic Errors

// SemErr logs a semantic error
func (el *ErrorListener) SemErr(
	offendingToken antlr.Token,
	msg string,
) {
	line := offendingToken.GetLine()
	column := offendingToken.GetColumn()
	el.SemanticError(line, column, msg)
}

// SemanticError logs a semantic error
func (el *ErrorListener) SemanticError(
	line, column int,
	msg string,
) {
	el.Diagnostics = append(el.Diagnostics, Diagnostic{"semantic", line, column, msg})
}
