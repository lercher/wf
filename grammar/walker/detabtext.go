package walker

import (
	"bufio"
	"bytes"
	"strings"
)

// detabText removes common indentation from a multiline string.
// It must be all indented either with spaces or tabs. If they are mixed
// the bahaviour is unpredicatable
func detabText(s string) string {
	if strings.IndexByte(s, '\n') == -1 {
		return s
	}
	countWS := len(s)
	scanner := bufio.NewScanner(strings.NewReader(s))
	for scanner.Scan() {
		line := []byte(scanner.Text())
		if len(line) != 0 {
			n := 0
			for {
				if n >= len(line) {
					break
				}
				if line[n] != ' ' && line[n] != '\t' {
					break
				}
				n++
			}
			if n < countWS {
				countWS = n
			}
		}
	}
	if countWS == 0 {
		return s // nothing to do
	}

	var buf bytes.Buffer
	scanner = bufio.NewScanner(strings.NewReader(s))
	for scanner.Scan() {
		line := []byte(scanner.Text())
		if countWS <= len(line) {
			buf.Write(line[countWS:])
		} else {
			buf.Write(line)
		}
		buf.WriteByte('\n')
	}
	return buf.String()
}
