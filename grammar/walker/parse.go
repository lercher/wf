package walker

import (
	"fmt"
	"io"
	"io/ioutil"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/lercher/wf/grammar/parser"
)

// Parse parses an FSE syntax and returns a semantic tree
func Parse(r io.Reader) (*Workflow, []Diagnostic, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, nil, err
	}

	input := antlr.NewInputStream(string(b)) // or NewFileStream
	lexer := parser.NewFSELexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewFSEParser(stream)
	p.Interpreter.SetPredictionMode(antlr.PredictionModeSLL)

	// p has a default error listener that prints unwanted diagnostics, so we remove it first:
	el := &ErrorListener{DiagnosticErrorListener: antlr.NewDiagnosticErrorListener(true)}
	p.RemoveErrorListeners()
	p.AddErrorListener(el)
	p.BuildParseTrees = true

	// now parse the FSE source reader
	tree := p.Main()
	if el.ErrorCount() > 0 {
		return nil, el.Diagnostics, fmt.Errorf("%d syntax error(s) detected", el.ErrorCount())
	}

	// create the AST by a walk of the parse tree
	w := newWlk(el, p.GetTokenStream())
	antlr.ParseTreeWalkerDefault.Walk(w, tree)
	if el.ErrorCount() > 0 {
		return nil, el.Diagnostics, fmt.Errorf("%d semantic error(s) detected", el.ErrorCount())
	}

	return &w.wf, nil, nil
}
