package walker

import "testing"

func TestDetabTextSingle(t *testing.T) {
	if got, want := detabText(""), ""; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
	if got, want := detabText("a word"), "a word"; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
	if got, want := detabText("  just a simple string "), "  just a simple string "; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

const (
	t1 = "\n\ta\r\n\t\tb\n\t\t\tc\n\ta\n\t"
	w1 = "\na\n\tb\n\t\tc\na\n\n"

	t2 = "\n  a\r\n    b\n  a\n  a"
	w2 = "\na\n  b\na\na\n"
)

func TestDetabTextMultiline(t *testing.T) {
	if got, want := detabText(t1), w1; got != want {
		t.Errorf("\nwant %q\n got %q", want, got)
	}
	if got, want := detabText(t2), w2; got != want {
		t.Errorf("\nwant %q\n got %q", want, got)
	}
}
