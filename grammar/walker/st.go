package walker

// Named is an identifier with a displayname and some descriptive text
type Named struct {
	Name        string   `json:"name,omitempty"`
	Option      string   `json:"option,omitempty"`
	Display     string   `json:"display,omitempty"`
	Description string   `json:"description,omitempty"`
	Roles       []string `json:"roles,omitempty"`
}

// DisplayOrName returns Display if it is not empty, otherwise Name is returned
func (n Named) DisplayOrName() string {
	if n.Display != "" {
		return n.Display
	}
	return n.Name
}

// Workflow is the parsed SemanticTree
type Workflow struct {
	Named
	States    []State           `json:"states,omitempty"`
	AllStates map[string]*State `json:"-"`
}

// State is a workflow state
type State struct {
	Named
	Line   int     `json:"line,omitempty"`
	Events []Event `json:"events,omitempty"`
	States []State `json:"states,omitempty"`
	Parent *State  `json:"-"`
}

// Event is sth a state waits for and can be triggered by Name from the outside
type Event struct {
	Named
	Type         string        `json:"type,omitempty"`
	Parameter    string        `json:"parameter,omitempty"`
	Consequences []Consequence `json:"consequences,omitempty"`
}

// Consequence is an action that acts as a consequence of an Initialization, a time-out or an Event
type Consequence struct {
	Line       int         `json:"line,omitempty"`
	Action     string      `json:"action,omitempty"`
	Goto       string      `json:"goto,omitempty"`
	LVar       string      `json:"lvar,omitempty"`
	Operator   string      `json:"operator,omitempty"`
	Function   string      `json:"function,omitempty"`
	Parameters []Parameter `json:"parameters,omitempty"`
}

// Parameter is either a variable name or a string constant
type Parameter struct {
	Var  string `json:"var,omitempty"`
	Text string `json:"text,omitempty"`
}

// IntersectsWith is true if e.Roles is empty or
// otherwise if there is a non-empty intersection between e.Roles
// and the given roles
func (e Event) IntersectsWith(roles []string) bool {
	if len(e.Roles) == 0 {
		// empty "for" list matches all roles
		return true
	}
	for _, r1 := range e.Roles {
		for _, r2 := range roles {
			if r1 == r2 {
				return true
			}
		}
	}
	return false
}
