package main

// go run . -j sample.fse > sample.json

import (
	"encoding/json"
	"flag"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/lercher/wf/grammar/walker"
)

var (
	flagJSON = flag.Bool("j", false, "output parsed JSON to stdout")
)

func main() {
	os.Exit(main2())
}

func main2() int {
	defer func(t time.Time) {
		log.Println(time.Now().Sub(t), "total processing time")
	}(time.Now())

	flag.Parse()

	var r io.Reader
	switch {
	case flag.NArg() > 0 && flag.Arg(0) != "-":
		f, err := os.Open(flag.Arg(0))
		if err != nil {
			log.Println(err)
			return 1
		}
		defer f.Close()
		r = f
	default:
		r = os.Stdin
	}

	wf, diag, err := walker.Parse(r)
	if err != nil {
		for i := range diag {
			log.Println(diag[i])
		}
		log.Println(err)
		return 2
	}

	log.Printf("%s %q loaded successfully", wf.Name, wf.Display)
	log.Println(wf.Description)
	log.Printf("%v total states, %v top level states", len(wf.AllStates), len(wf.States))

	if *flagJSON {
		enc := json.NewEncoder(os.Stdout)
		enc.SetIndent("", "  ")
		err = enc.Encode(wf)
		if err != nil {
			log.Println(err)
			return 3
		}
	}

	return 0
}
