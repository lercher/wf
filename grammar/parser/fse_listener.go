// Code generated from c://git//src//gitlab.com//lercher//wf//grammar//FSE.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // FSE

import "github.com/antlr4-go/antlr/v4"

// FSEListener is a complete listener for a parse tree produced by FSEParser.
type FSEListener interface {
	antlr.ParseTreeListener

	// EnterMain is called when entering the main production.
	EnterMain(c *MainContext)

	// EnterHeader is called when entering the header production.
	EnterHeader(c *HeaderContext)

	// EnterNamed is called when entering the named production.
	EnterNamed(c *NamedContext)

	// EnterState is called when entering the state production.
	EnterState(c *StateContext)

	// EnterInitialize is called when entering the initialize production.
	EnterInitialize(c *InitializeContext)

	// EnterAfter is called when entering the after production.
	EnterAfter(c *AfterContext)

	// EnterEvent is called when entering the event production.
	EnterEvent(c *EventContext)

	// EnterConsequences is called when entering the consequences production.
	EnterConsequences(c *ConsequencesContext)

	// EnterConsequence is called when entering the consequence production.
	EnterConsequence(c *ConsequenceContext)

	// EnterAssign is called when entering the assign production.
	EnterAssign(c *AssignContext)

	// EnterFuncall is called when entering the funcall production.
	EnterFuncall(c *FuncallContext)

	// EnterCall is called when entering the call production.
	EnterCall(c *CallContext)

	// EnterParam is called when entering the param production.
	EnterParam(c *ParamContext)

	// EnterIfgoto is called when entering the ifgoto production.
	EnterIfgoto(c *IfgotoContext)

	// EnterTerminate is called when entering the terminate production.
	EnterTerminate(c *TerminateContext)

	// ExitMain is called when exiting the main production.
	ExitMain(c *MainContext)

	// ExitHeader is called when exiting the header production.
	ExitHeader(c *HeaderContext)

	// ExitNamed is called when exiting the named production.
	ExitNamed(c *NamedContext)

	// ExitState is called when exiting the state production.
	ExitState(c *StateContext)

	// ExitInitialize is called when exiting the initialize production.
	ExitInitialize(c *InitializeContext)

	// ExitAfter is called when exiting the after production.
	ExitAfter(c *AfterContext)

	// ExitEvent is called when exiting the event production.
	ExitEvent(c *EventContext)

	// ExitConsequences is called when exiting the consequences production.
	ExitConsequences(c *ConsequencesContext)

	// ExitConsequence is called when exiting the consequence production.
	ExitConsequence(c *ConsequenceContext)

	// ExitAssign is called when exiting the assign production.
	ExitAssign(c *AssignContext)

	// ExitFuncall is called when exiting the funcall production.
	ExitFuncall(c *FuncallContext)

	// ExitCall is called when exiting the call production.
	ExitCall(c *CallContext)

	// ExitParam is called when exiting the param production.
	ExitParam(c *ParamContext)

	// ExitIfgoto is called when exiting the ifgoto production.
	ExitIfgoto(c *IfgotoContext)

	// ExitTerminate is called when exiting the terminate production.
	ExitTerminate(c *TerminateContext)
}
