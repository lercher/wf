// Code generated from c://git//src//gitlab.com//lercher//wf//grammar//FSE.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // FSE

import "github.com/antlr4-go/antlr/v4"

// BaseFSEListener is a complete listener for a parse tree produced by FSEParser.
type BaseFSEListener struct{}

var _ FSEListener = &BaseFSEListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseFSEListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseFSEListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseFSEListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseFSEListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterMain is called when production main is entered.
func (s *BaseFSEListener) EnterMain(ctx *MainContext) {}

// ExitMain is called when production main is exited.
func (s *BaseFSEListener) ExitMain(ctx *MainContext) {}

// EnterHeader is called when production header is entered.
func (s *BaseFSEListener) EnterHeader(ctx *HeaderContext) {}

// ExitHeader is called when production header is exited.
func (s *BaseFSEListener) ExitHeader(ctx *HeaderContext) {}

// EnterNamed is called when production named is entered.
func (s *BaseFSEListener) EnterNamed(ctx *NamedContext) {}

// ExitNamed is called when production named is exited.
func (s *BaseFSEListener) ExitNamed(ctx *NamedContext) {}

// EnterState is called when production state is entered.
func (s *BaseFSEListener) EnterState(ctx *StateContext) {}

// ExitState is called when production state is exited.
func (s *BaseFSEListener) ExitState(ctx *StateContext) {}

// EnterInitialize is called when production initialize is entered.
func (s *BaseFSEListener) EnterInitialize(ctx *InitializeContext) {}

// ExitInitialize is called when production initialize is exited.
func (s *BaseFSEListener) ExitInitialize(ctx *InitializeContext) {}

// EnterAfter is called when production after is entered.
func (s *BaseFSEListener) EnterAfter(ctx *AfterContext) {}

// ExitAfter is called when production after is exited.
func (s *BaseFSEListener) ExitAfter(ctx *AfterContext) {}

// EnterEvent is called when production event is entered.
func (s *BaseFSEListener) EnterEvent(ctx *EventContext) {}

// ExitEvent is called when production event is exited.
func (s *BaseFSEListener) ExitEvent(ctx *EventContext) {}

// EnterConsequences is called when production consequences is entered.
func (s *BaseFSEListener) EnterConsequences(ctx *ConsequencesContext) {}

// ExitConsequences is called when production consequences is exited.
func (s *BaseFSEListener) ExitConsequences(ctx *ConsequencesContext) {}

// EnterConsequence is called when production consequence is entered.
func (s *BaseFSEListener) EnterConsequence(ctx *ConsequenceContext) {}

// ExitConsequence is called when production consequence is exited.
func (s *BaseFSEListener) ExitConsequence(ctx *ConsequenceContext) {}

// EnterAssign is called when production assign is entered.
func (s *BaseFSEListener) EnterAssign(ctx *AssignContext) {}

// ExitAssign is called when production assign is exited.
func (s *BaseFSEListener) ExitAssign(ctx *AssignContext) {}

// EnterFuncall is called when production funcall is entered.
func (s *BaseFSEListener) EnterFuncall(ctx *FuncallContext) {}

// ExitFuncall is called when production funcall is exited.
func (s *BaseFSEListener) ExitFuncall(ctx *FuncallContext) {}

// EnterCall is called when production call is entered.
func (s *BaseFSEListener) EnterCall(ctx *CallContext) {}

// ExitCall is called when production call is exited.
func (s *BaseFSEListener) ExitCall(ctx *CallContext) {}

// EnterParam is called when production param is entered.
func (s *BaseFSEListener) EnterParam(ctx *ParamContext) {}

// ExitParam is called when production param is exited.
func (s *BaseFSEListener) ExitParam(ctx *ParamContext) {}

// EnterIfgoto is called when production ifgoto is entered.
func (s *BaseFSEListener) EnterIfgoto(ctx *IfgotoContext) {}

// ExitIfgoto is called when production ifgoto is exited.
func (s *BaseFSEListener) ExitIfgoto(ctx *IfgotoContext) {}

// EnterTerminate is called when production terminate is entered.
func (s *BaseFSEListener) EnterTerminate(ctx *TerminateContext) {}

// ExitTerminate is called when production terminate is exited.
func (s *BaseFSEListener) ExitTerminate(ctx *TerminateContext) {}
