// Code generated from c://git//src//gitlab.com//lercher//wf//grammar//FSE.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // FSE

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type FSEParser struct {
	*antlr.BaseParser
}

var FSEParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func fseParserInit() {
	staticData := &FSEParserStaticData
	staticData.LiteralNames = []string{
		"", "'workflow'", "':'", "'as'", "'for'", "','", "'state'", "'{'", "'}'",
		"'initialize'", "'after'", "'event'", "';'", "'='", "'('", "')'", "'if'",
		"'=='", "'!='", "'goto'", "'halt'", "'terminate'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "COMMENT", "LINE_COMMENT", "WS", "NAME", "VAR",
		"TEXT", "DURATION", "DESCRIPTION", "ERRORCHARACTER",
	}
	staticData.RuleNames = []string{
		"main", "header", "named", "state", "initialize", "after", "event",
		"consequences", "consequence", "assign", "funcall", "call", "param",
		"ifgoto", "terminate",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 30, 152, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7,
		4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2, 10, 7,
		10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 1, 0, 1, 0,
		4, 0, 33, 8, 0, 11, 0, 12, 0, 34, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1,
		2, 1, 2, 3, 2, 45, 8, 2, 1, 2, 1, 2, 3, 2, 49, 8, 2, 1, 2, 1, 2, 1, 2,
		3, 2, 54, 8, 2, 1, 2, 5, 2, 57, 8, 2, 10, 2, 12, 2, 60, 9, 2, 3, 2, 62,
		8, 2, 1, 2, 3, 2, 65, 8, 2, 1, 3, 1, 3, 1, 3, 1, 3, 3, 3, 71, 8, 3, 1,
		3, 3, 3, 74, 8, 3, 1, 3, 5, 3, 77, 8, 3, 10, 3, 12, 3, 80, 9, 3, 1, 3,
		5, 3, 83, 8, 3, 10, 3, 12, 3, 86, 9, 3, 1, 3, 1, 3, 1, 4, 1, 4, 1, 4, 1,
		5, 1, 5, 1, 5, 1, 5, 1, 6, 1, 6, 1, 6, 1, 6, 1, 7, 5, 7, 102, 8, 7, 10,
		7, 12, 7, 105, 9, 7, 1, 7, 1, 7, 1, 8, 1, 8, 1, 8, 1, 8, 3, 8, 113, 8,
		8, 1, 9, 1, 9, 1, 9, 1, 9, 3, 9, 119, 8, 9, 1, 10, 1, 10, 1, 11, 1, 11,
		1, 11, 5, 11, 126, 8, 11, 10, 11, 12, 11, 129, 9, 11, 1, 11, 1, 11, 1,
		12, 1, 12, 3, 12, 135, 8, 12, 1, 13, 1, 13, 1, 13, 1, 13, 3, 13, 141, 8,
		13, 3, 13, 143, 8, 13, 1, 13, 1, 13, 1, 13, 3, 13, 148, 8, 13, 1, 14, 1,
		14, 1, 14, 0, 0, 15, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26,
		28, 0, 2, 1, 0, 17, 18, 1, 0, 25, 26, 157, 0, 30, 1, 0, 0, 0, 2, 38, 1,
		0, 0, 0, 4, 41, 1, 0, 0, 0, 6, 66, 1, 0, 0, 0, 8, 89, 1, 0, 0, 0, 10, 92,
		1, 0, 0, 0, 12, 96, 1, 0, 0, 0, 14, 103, 1, 0, 0, 0, 16, 112, 1, 0, 0,
		0, 18, 114, 1, 0, 0, 0, 20, 120, 1, 0, 0, 0, 22, 122, 1, 0, 0, 0, 24, 134,
		1, 0, 0, 0, 26, 142, 1, 0, 0, 0, 28, 149, 1, 0, 0, 0, 30, 32, 3, 2, 1,
		0, 31, 33, 3, 6, 3, 0, 32, 31, 1, 0, 0, 0, 33, 34, 1, 0, 0, 0, 34, 32,
		1, 0, 0, 0, 34, 35, 1, 0, 0, 0, 35, 36, 1, 0, 0, 0, 36, 37, 5, 0, 0, 1,
		37, 1, 1, 0, 0, 0, 38, 39, 5, 1, 0, 0, 39, 40, 3, 4, 2, 0, 40, 3, 1, 0,
		0, 0, 41, 44, 5, 25, 0, 0, 42, 43, 5, 2, 0, 0, 43, 45, 5, 27, 0, 0, 44,
		42, 1, 0, 0, 0, 44, 45, 1, 0, 0, 0, 45, 48, 1, 0, 0, 0, 46, 47, 5, 3, 0,
		0, 47, 49, 5, 27, 0, 0, 48, 46, 1, 0, 0, 0, 48, 49, 1, 0, 0, 0, 49, 61,
		1, 0, 0, 0, 50, 51, 5, 4, 0, 0, 51, 58, 5, 27, 0, 0, 52, 54, 5, 5, 0, 0,
		53, 52, 1, 0, 0, 0, 53, 54, 1, 0, 0, 0, 54, 55, 1, 0, 0, 0, 55, 57, 5,
		27, 0, 0, 56, 53, 1, 0, 0, 0, 57, 60, 1, 0, 0, 0, 58, 56, 1, 0, 0, 0, 58,
		59, 1, 0, 0, 0, 59, 62, 1, 0, 0, 0, 60, 58, 1, 0, 0, 0, 61, 50, 1, 0, 0,
		0, 61, 62, 1, 0, 0, 0, 62, 64, 1, 0, 0, 0, 63, 65, 5, 29, 0, 0, 64, 63,
		1, 0, 0, 0, 64, 65, 1, 0, 0, 0, 65, 5, 1, 0, 0, 0, 66, 67, 5, 6, 0, 0,
		67, 68, 3, 4, 2, 0, 68, 70, 5, 7, 0, 0, 69, 71, 3, 8, 4, 0, 70, 69, 1,
		0, 0, 0, 70, 71, 1, 0, 0, 0, 71, 73, 1, 0, 0, 0, 72, 74, 3, 10, 5, 0, 73,
		72, 1, 0, 0, 0, 73, 74, 1, 0, 0, 0, 74, 78, 1, 0, 0, 0, 75, 77, 3, 12,
		6, 0, 76, 75, 1, 0, 0, 0, 77, 80, 1, 0, 0, 0, 78, 76, 1, 0, 0, 0, 78, 79,
		1, 0, 0, 0, 79, 84, 1, 0, 0, 0, 80, 78, 1, 0, 0, 0, 81, 83, 3, 6, 3, 0,
		82, 81, 1, 0, 0, 0, 83, 86, 1, 0, 0, 0, 84, 82, 1, 0, 0, 0, 84, 85, 1,
		0, 0, 0, 85, 87, 1, 0, 0, 0, 86, 84, 1, 0, 0, 0, 87, 88, 5, 8, 0, 0, 88,
		7, 1, 0, 0, 0, 89, 90, 5, 9, 0, 0, 90, 91, 3, 14, 7, 0, 91, 9, 1, 0, 0,
		0, 92, 93, 5, 10, 0, 0, 93, 94, 5, 28, 0, 0, 94, 95, 3, 14, 7, 0, 95, 11,
		1, 0, 0, 0, 96, 97, 5, 11, 0, 0, 97, 98, 3, 4, 2, 0, 98, 99, 3, 14, 7,
		0, 99, 13, 1, 0, 0, 0, 100, 102, 3, 16, 8, 0, 101, 100, 1, 0, 0, 0, 102,
		105, 1, 0, 0, 0, 103, 101, 1, 0, 0, 0, 103, 104, 1, 0, 0, 0, 104, 106,
		1, 0, 0, 0, 105, 103, 1, 0, 0, 0, 106, 107, 5, 12, 0, 0, 107, 15, 1, 0,
		0, 0, 108, 113, 3, 18, 9, 0, 109, 113, 3, 20, 10, 0, 110, 113, 3, 26, 13,
		0, 111, 113, 3, 28, 14, 0, 112, 108, 1, 0, 0, 0, 112, 109, 1, 0, 0, 0,
		112, 110, 1, 0, 0, 0, 112, 111, 1, 0, 0, 0, 113, 17, 1, 0, 0, 0, 114, 115,
		5, 26, 0, 0, 115, 118, 5, 13, 0, 0, 116, 119, 3, 24, 12, 0, 117, 119, 3,
		22, 11, 0, 118, 116, 1, 0, 0, 0, 118, 117, 1, 0, 0, 0, 119, 19, 1, 0, 0,
		0, 120, 121, 3, 22, 11, 0, 121, 21, 1, 0, 0, 0, 122, 123, 5, 25, 0, 0,
		123, 127, 5, 14, 0, 0, 124, 126, 3, 24, 12, 0, 125, 124, 1, 0, 0, 0, 126,
		129, 1, 0, 0, 0, 127, 125, 1, 0, 0, 0, 127, 128, 1, 0, 0, 0, 128, 130,
		1, 0, 0, 0, 129, 127, 1, 0, 0, 0, 130, 131, 5, 15, 0, 0, 131, 23, 1, 0,
		0, 0, 132, 135, 5, 27, 0, 0, 133, 135, 5, 26, 0, 0, 134, 132, 1, 0, 0,
		0, 134, 133, 1, 0, 0, 0, 135, 25, 1, 0, 0, 0, 136, 137, 5, 16, 0, 0, 137,
		140, 5, 26, 0, 0, 138, 139, 7, 0, 0, 0, 139, 141, 3, 24, 12, 0, 140, 138,
		1, 0, 0, 0, 140, 141, 1, 0, 0, 0, 141, 143, 1, 0, 0, 0, 142, 136, 1, 0,
		0, 0, 142, 143, 1, 0, 0, 0, 143, 147, 1, 0, 0, 0, 144, 145, 5, 19, 0, 0,
		145, 148, 7, 1, 0, 0, 146, 148, 5, 20, 0, 0, 147, 144, 1, 0, 0, 0, 147,
		146, 1, 0, 0, 0, 148, 27, 1, 0, 0, 0, 149, 150, 5, 21, 0, 0, 150, 29, 1,
		0, 0, 0, 19, 34, 44, 48, 53, 58, 61, 64, 70, 73, 78, 84, 103, 112, 118,
		127, 134, 140, 142, 147,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// FSEParserInit initializes any static state used to implement FSEParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewFSEParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func FSEParserInit() {
	staticData := &FSEParserStaticData
	staticData.once.Do(fseParserInit)
}

// NewFSEParser produces a new parser instance for the optional input antlr.TokenStream.
func NewFSEParser(input antlr.TokenStream) *FSEParser {
	FSEParserInit()
	this := new(FSEParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &FSEParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "FSE.g4"

	return this
}

// FSEParser tokens.
const (
	FSEParserEOF            = antlr.TokenEOF
	FSEParserT__0           = 1
	FSEParserT__1           = 2
	FSEParserT__2           = 3
	FSEParserT__3           = 4
	FSEParserT__4           = 5
	FSEParserT__5           = 6
	FSEParserT__6           = 7
	FSEParserT__7           = 8
	FSEParserT__8           = 9
	FSEParserT__9           = 10
	FSEParserT__10          = 11
	FSEParserT__11          = 12
	FSEParserT__12          = 13
	FSEParserT__13          = 14
	FSEParserT__14          = 15
	FSEParserT__15          = 16
	FSEParserT__16          = 17
	FSEParserT__17          = 18
	FSEParserT__18          = 19
	FSEParserT__19          = 20
	FSEParserT__20          = 21
	FSEParserCOMMENT        = 22
	FSEParserLINE_COMMENT   = 23
	FSEParserWS             = 24
	FSEParserNAME           = 25
	FSEParserVAR            = 26
	FSEParserTEXT           = 27
	FSEParserDURATION       = 28
	FSEParserDESCRIPTION    = 29
	FSEParserERRORCHARACTER = 30
)

// FSEParser rules.
const (
	FSEParserRULE_main         = 0
	FSEParserRULE_header       = 1
	FSEParserRULE_named        = 2
	FSEParserRULE_state        = 3
	FSEParserRULE_initialize   = 4
	FSEParserRULE_after        = 5
	FSEParserRULE_event        = 6
	FSEParserRULE_consequences = 7
	FSEParserRULE_consequence  = 8
	FSEParserRULE_assign       = 9
	FSEParserRULE_funcall      = 10
	FSEParserRULE_call         = 11
	FSEParserRULE_param        = 12
	FSEParserRULE_ifgoto       = 13
	FSEParserRULE_terminate    = 14
)

// IMainContext is an interface to support dynamic dispatch.
type IMainContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Header() IHeaderContext
	EOF() antlr.TerminalNode
	AllState() []IStateContext
	State(i int) IStateContext

	// IsMainContext differentiates from other interfaces.
	IsMainContext()
}

type MainContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMainContext() *MainContext {
	var p = new(MainContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_main
	return p
}

func InitEmptyMainContext(p *MainContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_main
}

func (*MainContext) IsMainContext() {}

func NewMainContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MainContext {
	var p = new(MainContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_main

	return p
}

func (s *MainContext) GetParser() antlr.Parser { return s.parser }

func (s *MainContext) Header() IHeaderContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IHeaderContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IHeaderContext)
}

func (s *MainContext) EOF() antlr.TerminalNode {
	return s.GetToken(FSEParserEOF, 0)
}

func (s *MainContext) AllState() []IStateContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IStateContext); ok {
			len++
		}
	}

	tst := make([]IStateContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IStateContext); ok {
			tst[i] = t.(IStateContext)
			i++
		}
	}

	return tst
}

func (s *MainContext) State(i int) IStateContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStateContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStateContext)
}

func (s *MainContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MainContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MainContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterMain(s)
	}
}

func (s *MainContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitMain(s)
	}
}

func (p *FSEParser) Main() (localctx IMainContext) {
	localctx = NewMainContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, FSEParserRULE_main)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(30)
		p.Header()
	}
	p.SetState(32)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = _la == FSEParserT__5 {
		{
			p.SetState(31)
			p.State()
		}

		p.SetState(34)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(36)
		p.Match(FSEParserEOF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IHeaderContext is an interface to support dynamic dispatch.
type IHeaderContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Named() INamedContext

	// IsHeaderContext differentiates from other interfaces.
	IsHeaderContext()
}

type HeaderContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyHeaderContext() *HeaderContext {
	var p = new(HeaderContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_header
	return p
}

func InitEmptyHeaderContext(p *HeaderContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_header
}

func (*HeaderContext) IsHeaderContext() {}

func NewHeaderContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *HeaderContext {
	var p = new(HeaderContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_header

	return p
}

func (s *HeaderContext) GetParser() antlr.Parser { return s.parser }

func (s *HeaderContext) Named() INamedContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(INamedContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(INamedContext)
}

func (s *HeaderContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *HeaderContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *HeaderContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterHeader(s)
	}
}

func (s *HeaderContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitHeader(s)
	}
}

func (p *FSEParser) Header() (localctx IHeaderContext) {
	localctx = NewHeaderContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, FSEParserRULE_header)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(38)
		p.Match(FSEParserT__0)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(39)
		p.Named()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// INamedContext is an interface to support dynamic dispatch.
type INamedContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name token.
	GetName() antlr.Token

	// GetOption returns the option token.
	GetOption() antlr.Token

	// GetDisplay returns the display token.
	GetDisplay() antlr.Token

	// GetLnames returns the lnames token.
	GetLnames() antlr.Token

	// GetRnames returns the rnames token.
	GetRnames() antlr.Token

	// GetDescription returns the description token.
	GetDescription() antlr.Token

	// SetName sets the name token.
	SetName(antlr.Token)

	// SetOption sets the option token.
	SetOption(antlr.Token)

	// SetDisplay sets the display token.
	SetDisplay(antlr.Token)

	// SetLnames sets the lnames token.
	SetLnames(antlr.Token)

	// SetRnames sets the rnames token.
	SetRnames(antlr.Token)

	// SetDescription sets the description token.
	SetDescription(antlr.Token)

	// Getter signatures
	NAME() antlr.TerminalNode
	AllTEXT() []antlr.TerminalNode
	TEXT(i int) antlr.TerminalNode
	DESCRIPTION() antlr.TerminalNode

	// IsNamedContext differentiates from other interfaces.
	IsNamedContext()
}

type NamedContext struct {
	antlr.BaseParserRuleContext
	parser      antlr.Parser
	name        antlr.Token
	option      antlr.Token
	display     antlr.Token
	lnames      antlr.Token
	rnames      antlr.Token
	description antlr.Token
}

func NewEmptyNamedContext() *NamedContext {
	var p = new(NamedContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_named
	return p
}

func InitEmptyNamedContext(p *NamedContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_named
}

func (*NamedContext) IsNamedContext() {}

func NewNamedContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *NamedContext {
	var p = new(NamedContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_named

	return p
}

func (s *NamedContext) GetParser() antlr.Parser { return s.parser }

func (s *NamedContext) GetName() antlr.Token { return s.name }

func (s *NamedContext) GetOption() antlr.Token { return s.option }

func (s *NamedContext) GetDisplay() antlr.Token { return s.display }

func (s *NamedContext) GetLnames() antlr.Token { return s.lnames }

func (s *NamedContext) GetRnames() antlr.Token { return s.rnames }

func (s *NamedContext) GetDescription() antlr.Token { return s.description }

func (s *NamedContext) SetName(v antlr.Token) { s.name = v }

func (s *NamedContext) SetOption(v antlr.Token) { s.option = v }

func (s *NamedContext) SetDisplay(v antlr.Token) { s.display = v }

func (s *NamedContext) SetLnames(v antlr.Token) { s.lnames = v }

func (s *NamedContext) SetRnames(v antlr.Token) { s.rnames = v }

func (s *NamedContext) SetDescription(v antlr.Token) { s.description = v }

func (s *NamedContext) NAME() antlr.TerminalNode {
	return s.GetToken(FSEParserNAME, 0)
}

func (s *NamedContext) AllTEXT() []antlr.TerminalNode {
	return s.GetTokens(FSEParserTEXT)
}

func (s *NamedContext) TEXT(i int) antlr.TerminalNode {
	return s.GetToken(FSEParserTEXT, i)
}

func (s *NamedContext) DESCRIPTION() antlr.TerminalNode {
	return s.GetToken(FSEParserDESCRIPTION, 0)
}

func (s *NamedContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NamedContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *NamedContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterNamed(s)
	}
}

func (s *NamedContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitNamed(s)
	}
}

func (p *FSEParser) Named() (localctx INamedContext) {
	localctx = NewNamedContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, FSEParserRULE_named)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(41)

		var _m = p.Match(FSEParserNAME)

		localctx.(*NamedContext).name = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(44)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__1 {
		{
			p.SetState(42)
			p.Match(FSEParserT__1)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(43)

			var _m = p.Match(FSEParserTEXT)

			localctx.(*NamedContext).option = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	p.SetState(48)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__2 {
		{
			p.SetState(46)
			p.Match(FSEParserT__2)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(47)

			var _m = p.Match(FSEParserTEXT)

			localctx.(*NamedContext).display = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	p.SetState(61)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__3 {
		{
			p.SetState(50)
			p.Match(FSEParserT__3)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(51)

			var _m = p.Match(FSEParserTEXT)

			localctx.(*NamedContext).lnames = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(58)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		for _la == FSEParserT__4 || _la == FSEParserTEXT {
			p.SetState(53)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)

			if _la == FSEParserT__4 {
				{
					p.SetState(52)
					p.Match(FSEParserT__4)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}

			}
			{
				p.SetState(55)

				var _m = p.Match(FSEParserTEXT)

				localctx.(*NamedContext).rnames = _m
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

			p.SetState(60)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)
		}

	}
	p.SetState(64)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserDESCRIPTION {
		{
			p.SetState(63)

			var _m = p.Match(FSEParserDESCRIPTION)

			localctx.(*NamedContext).description = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStateContext is an interface to support dynamic dispatch.
type IStateContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Named() INamedContext
	Initialize() IInitializeContext
	After() IAfterContext
	AllEvent() []IEventContext
	Event(i int) IEventContext
	AllState() []IStateContext
	State(i int) IStateContext

	// IsStateContext differentiates from other interfaces.
	IsStateContext()
}

type StateContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStateContext() *StateContext {
	var p = new(StateContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_state
	return p
}

func InitEmptyStateContext(p *StateContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_state
}

func (*StateContext) IsStateContext() {}

func NewStateContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StateContext {
	var p = new(StateContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_state

	return p
}

func (s *StateContext) GetParser() antlr.Parser { return s.parser }

func (s *StateContext) Named() INamedContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(INamedContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(INamedContext)
}

func (s *StateContext) Initialize() IInitializeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IInitializeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IInitializeContext)
}

func (s *StateContext) After() IAfterContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IAfterContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IAfterContext)
}

func (s *StateContext) AllEvent() []IEventContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEventContext); ok {
			len++
		}
	}

	tst := make([]IEventContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEventContext); ok {
			tst[i] = t.(IEventContext)
			i++
		}
	}

	return tst
}

func (s *StateContext) Event(i int) IEventContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEventContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEventContext)
}

func (s *StateContext) AllState() []IStateContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IStateContext); ok {
			len++
		}
	}

	tst := make([]IStateContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IStateContext); ok {
			tst[i] = t.(IStateContext)
			i++
		}
	}

	return tst
}

func (s *StateContext) State(i int) IStateContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStateContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStateContext)
}

func (s *StateContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StateContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StateContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterState(s)
	}
}

func (s *StateContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitState(s)
	}
}

func (p *FSEParser) State() (localctx IStateContext) {
	localctx = NewStateContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, FSEParserRULE_state)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(66)
		p.Match(FSEParserT__5)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(67)
		p.Named()
	}
	{
		p.SetState(68)
		p.Match(FSEParserT__6)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(70)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__8 {
		{
			p.SetState(69)
			p.Initialize()
		}

	}
	p.SetState(73)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__9 {
		{
			p.SetState(72)
			p.After()
		}

	}
	p.SetState(78)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == FSEParserT__10 {
		{
			p.SetState(75)
			p.Event()
		}

		p.SetState(80)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	p.SetState(84)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == FSEParserT__5 {
		{
			p.SetState(81)
			p.State()
		}

		p.SetState(86)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(87)
		p.Match(FSEParserT__7)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IInitializeContext is an interface to support dynamic dispatch.
type IInitializeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Consequences() IConsequencesContext

	// IsInitializeContext differentiates from other interfaces.
	IsInitializeContext()
}

type InitializeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyInitializeContext() *InitializeContext {
	var p = new(InitializeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_initialize
	return p
}

func InitEmptyInitializeContext(p *InitializeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_initialize
}

func (*InitializeContext) IsInitializeContext() {}

func NewInitializeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *InitializeContext {
	var p = new(InitializeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_initialize

	return p
}

func (s *InitializeContext) GetParser() antlr.Parser { return s.parser }

func (s *InitializeContext) Consequences() IConsequencesContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IConsequencesContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IConsequencesContext)
}

func (s *InitializeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *InitializeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *InitializeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterInitialize(s)
	}
}

func (s *InitializeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitInitialize(s)
	}
}

func (p *FSEParser) Initialize() (localctx IInitializeContext) {
	localctx = NewInitializeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, FSEParserRULE_initialize)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(89)
		p.Match(FSEParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(90)
		p.Consequences()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IAfterContext is an interface to support dynamic dispatch.
type IAfterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetDuration returns the duration token.
	GetDuration() antlr.Token

	// SetDuration sets the duration token.
	SetDuration(antlr.Token)

	// Getter signatures
	Consequences() IConsequencesContext
	DURATION() antlr.TerminalNode

	// IsAfterContext differentiates from other interfaces.
	IsAfterContext()
}

type AfterContext struct {
	antlr.BaseParserRuleContext
	parser   antlr.Parser
	duration antlr.Token
}

func NewEmptyAfterContext() *AfterContext {
	var p = new(AfterContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_after
	return p
}

func InitEmptyAfterContext(p *AfterContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_after
}

func (*AfterContext) IsAfterContext() {}

func NewAfterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AfterContext {
	var p = new(AfterContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_after

	return p
}

func (s *AfterContext) GetParser() antlr.Parser { return s.parser }

func (s *AfterContext) GetDuration() antlr.Token { return s.duration }

func (s *AfterContext) SetDuration(v antlr.Token) { s.duration = v }

func (s *AfterContext) Consequences() IConsequencesContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IConsequencesContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IConsequencesContext)
}

func (s *AfterContext) DURATION() antlr.TerminalNode {
	return s.GetToken(FSEParserDURATION, 0)
}

func (s *AfterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AfterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AfterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterAfter(s)
	}
}

func (s *AfterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitAfter(s)
	}
}

func (p *FSEParser) After() (localctx IAfterContext) {
	localctx = NewAfterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, FSEParserRULE_after)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(92)
		p.Match(FSEParserT__9)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(93)

		var _m = p.Match(FSEParserDURATION)

		localctx.(*AfterContext).duration = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(94)
		p.Consequences()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEventContext is an interface to support dynamic dispatch.
type IEventContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Named() INamedContext
	Consequences() IConsequencesContext

	// IsEventContext differentiates from other interfaces.
	IsEventContext()
}

type EventContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEventContext() *EventContext {
	var p = new(EventContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_event
	return p
}

func InitEmptyEventContext(p *EventContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_event
}

func (*EventContext) IsEventContext() {}

func NewEventContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EventContext {
	var p = new(EventContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_event

	return p
}

func (s *EventContext) GetParser() antlr.Parser { return s.parser }

func (s *EventContext) Named() INamedContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(INamedContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(INamedContext)
}

func (s *EventContext) Consequences() IConsequencesContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IConsequencesContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IConsequencesContext)
}

func (s *EventContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EventContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EventContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterEvent(s)
	}
}

func (s *EventContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitEvent(s)
	}
}

func (p *FSEParser) Event() (localctx IEventContext) {
	localctx = NewEventContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, FSEParserRULE_event)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(96)
		p.Match(FSEParserT__10)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(97)
		p.Named()
	}
	{
		p.SetState(98)
		p.Consequences()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IConsequencesContext is an interface to support dynamic dispatch.
type IConsequencesContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetCs returns the cs rule contexts.
	GetCs() IConsequenceContext

	// SetCs sets the cs rule contexts.
	SetCs(IConsequenceContext)

	// Getter signatures
	AllConsequence() []IConsequenceContext
	Consequence(i int) IConsequenceContext

	// IsConsequencesContext differentiates from other interfaces.
	IsConsequencesContext()
}

type ConsequencesContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	cs     IConsequenceContext
}

func NewEmptyConsequencesContext() *ConsequencesContext {
	var p = new(ConsequencesContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_consequences
	return p
}

func InitEmptyConsequencesContext(p *ConsequencesContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_consequences
}

func (*ConsequencesContext) IsConsequencesContext() {}

func NewConsequencesContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ConsequencesContext {
	var p = new(ConsequencesContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_consequences

	return p
}

func (s *ConsequencesContext) GetParser() antlr.Parser { return s.parser }

func (s *ConsequencesContext) GetCs() IConsequenceContext { return s.cs }

func (s *ConsequencesContext) SetCs(v IConsequenceContext) { s.cs = v }

func (s *ConsequencesContext) AllConsequence() []IConsequenceContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IConsequenceContext); ok {
			len++
		}
	}

	tst := make([]IConsequenceContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IConsequenceContext); ok {
			tst[i] = t.(IConsequenceContext)
			i++
		}
	}

	return tst
}

func (s *ConsequencesContext) Consequence(i int) IConsequenceContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IConsequenceContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IConsequenceContext)
}

func (s *ConsequencesContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ConsequencesContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ConsequencesContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterConsequences(s)
	}
}

func (s *ConsequencesContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitConsequences(s)
	}
}

func (p *FSEParser) Consequences() (localctx IConsequencesContext) {
	localctx = NewConsequencesContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, FSEParserRULE_consequences)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(103)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&104398848) != 0 {
		{
			p.SetState(100)

			var _x = p.Consequence()

			localctx.(*ConsequencesContext).cs = _x
		}

		p.SetState(105)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(106)
		p.Match(FSEParserT__11)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IConsequenceContext is an interface to support dynamic dispatch.
type IConsequenceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Assign() IAssignContext
	Funcall() IFuncallContext
	Ifgoto() IIfgotoContext
	Terminate() ITerminateContext

	// IsConsequenceContext differentiates from other interfaces.
	IsConsequenceContext()
}

type ConsequenceContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyConsequenceContext() *ConsequenceContext {
	var p = new(ConsequenceContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_consequence
	return p
}

func InitEmptyConsequenceContext(p *ConsequenceContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_consequence
}

func (*ConsequenceContext) IsConsequenceContext() {}

func NewConsequenceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ConsequenceContext {
	var p = new(ConsequenceContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_consequence

	return p
}

func (s *ConsequenceContext) GetParser() antlr.Parser { return s.parser }

func (s *ConsequenceContext) Assign() IAssignContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IAssignContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IAssignContext)
}

func (s *ConsequenceContext) Funcall() IFuncallContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFuncallContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFuncallContext)
}

func (s *ConsequenceContext) Ifgoto() IIfgotoContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIfgotoContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIfgotoContext)
}

func (s *ConsequenceContext) Terminate() ITerminateContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITerminateContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITerminateContext)
}

func (s *ConsequenceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ConsequenceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ConsequenceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterConsequence(s)
	}
}

func (s *ConsequenceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitConsequence(s)
	}
}

func (p *FSEParser) Consequence() (localctx IConsequenceContext) {
	localctx = NewConsequenceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, FSEParserRULE_consequence)
	p.EnterOuterAlt(localctx, 1)
	p.SetState(112)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case FSEParserVAR:
		{
			p.SetState(108)
			p.Assign()
		}

	case FSEParserNAME:
		{
			p.SetState(109)
			p.Funcall()
		}

	case FSEParserT__15, FSEParserT__18, FSEParserT__19:
		{
			p.SetState(110)
			p.Ifgoto()
		}

	case FSEParserT__20:
		{
			p.SetState(111)
			p.Terminate()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IAssignContext is an interface to support dynamic dispatch.
type IAssignContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetLvar returns the lvar token.
	GetLvar() antlr.Token

	// SetLvar sets the lvar token.
	SetLvar(antlr.Token)

	// Getter signatures
	VAR() antlr.TerminalNode
	Param() IParamContext
	Call() ICallContext

	// IsAssignContext differentiates from other interfaces.
	IsAssignContext()
}

type AssignContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	lvar   antlr.Token
}

func NewEmptyAssignContext() *AssignContext {
	var p = new(AssignContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_assign
	return p
}

func InitEmptyAssignContext(p *AssignContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_assign
}

func (*AssignContext) IsAssignContext() {}

func NewAssignContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AssignContext {
	var p = new(AssignContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_assign

	return p
}

func (s *AssignContext) GetParser() antlr.Parser { return s.parser }

func (s *AssignContext) GetLvar() antlr.Token { return s.lvar }

func (s *AssignContext) SetLvar(v antlr.Token) { s.lvar = v }

func (s *AssignContext) VAR() antlr.TerminalNode {
	return s.GetToken(FSEParserVAR, 0)
}

func (s *AssignContext) Param() IParamContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParamContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParamContext)
}

func (s *AssignContext) Call() ICallContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ICallContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *AssignContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AssignContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AssignContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterAssign(s)
	}
}

func (s *AssignContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitAssign(s)
	}
}

func (p *FSEParser) Assign() (localctx IAssignContext) {
	localctx = NewAssignContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, FSEParserRULE_assign)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(114)

		var _m = p.Match(FSEParserVAR)

		localctx.(*AssignContext).lvar = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(115)
		p.Match(FSEParserT__12)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(118)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case FSEParserVAR, FSEParserTEXT:
		{
			p.SetState(116)
			p.Param()
		}

	case FSEParserNAME:
		{
			p.SetState(117)
			p.Call()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IFuncallContext is an interface to support dynamic dispatch.
type IFuncallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Call() ICallContext

	// IsFuncallContext differentiates from other interfaces.
	IsFuncallContext()
}

type FuncallContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFuncallContext() *FuncallContext {
	var p = new(FuncallContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_funcall
	return p
}

func InitEmptyFuncallContext(p *FuncallContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_funcall
}

func (*FuncallContext) IsFuncallContext() {}

func NewFuncallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FuncallContext {
	var p = new(FuncallContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_funcall

	return p
}

func (s *FuncallContext) GetParser() antlr.Parser { return s.parser }

func (s *FuncallContext) Call() ICallContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ICallContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *FuncallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FuncallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FuncallContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterFuncall(s)
	}
}

func (s *FuncallContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitFuncall(s)
	}
}

func (p *FSEParser) Funcall() (localctx IFuncallContext) {
	localctx = NewFuncallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, FSEParserRULE_funcall)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(120)
		p.Call()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ICallContext is an interface to support dynamic dispatch.
type ICallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name token.
	GetName() antlr.Token

	// SetName sets the name token.
	SetName(antlr.Token)

	// Getter signatures
	NAME() antlr.TerminalNode
	AllParam() []IParamContext
	Param(i int) IParamContext

	// IsCallContext differentiates from other interfaces.
	IsCallContext()
}

type CallContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	name   antlr.Token
}

func NewEmptyCallContext() *CallContext {
	var p = new(CallContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_call
	return p
}

func InitEmptyCallContext(p *CallContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_call
}

func (*CallContext) IsCallContext() {}

func NewCallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallContext {
	var p = new(CallContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_call

	return p
}

func (s *CallContext) GetParser() antlr.Parser { return s.parser }

func (s *CallContext) GetName() antlr.Token { return s.name }

func (s *CallContext) SetName(v antlr.Token) { s.name = v }

func (s *CallContext) NAME() antlr.TerminalNode {
	return s.GetToken(FSEParserNAME, 0)
}

func (s *CallContext) AllParam() []IParamContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IParamContext); ok {
			len++
		}
	}

	tst := make([]IParamContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IParamContext); ok {
			tst[i] = t.(IParamContext)
			i++
		}
	}

	return tst
}

func (s *CallContext) Param(i int) IParamContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParamContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParamContext)
}

func (s *CallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterCall(s)
	}
}

func (s *CallContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitCall(s)
	}
}

func (p *FSEParser) Call() (localctx ICallContext) {
	localctx = NewCallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, FSEParserRULE_call)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(122)

		var _m = p.Match(FSEParserNAME)

		localctx.(*CallContext).name = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(123)
		p.Match(FSEParserT__13)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(127)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == FSEParserVAR || _la == FSEParserTEXT {
		{
			p.SetState(124)
			p.Param()
		}

		p.SetState(129)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(130)
		p.Match(FSEParserT__14)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IParamContext is an interface to support dynamic dispatch.
type IParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRtext returns the rtext token.
	GetRtext() antlr.Token

	// GetRvar returns the rvar token.
	GetRvar() antlr.Token

	// SetRtext sets the rtext token.
	SetRtext(antlr.Token)

	// SetRvar sets the rvar token.
	SetRvar(antlr.Token)

	// Getter signatures
	TEXT() antlr.TerminalNode
	VAR() antlr.TerminalNode

	// IsParamContext differentiates from other interfaces.
	IsParamContext()
}

type ParamContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	rtext  antlr.Token
	rvar   antlr.Token
}

func NewEmptyParamContext() *ParamContext {
	var p = new(ParamContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_param
	return p
}

func InitEmptyParamContext(p *ParamContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_param
}

func (*ParamContext) IsParamContext() {}

func NewParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ParamContext {
	var p = new(ParamContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_param

	return p
}

func (s *ParamContext) GetParser() antlr.Parser { return s.parser }

func (s *ParamContext) GetRtext() antlr.Token { return s.rtext }

func (s *ParamContext) GetRvar() antlr.Token { return s.rvar }

func (s *ParamContext) SetRtext(v antlr.Token) { s.rtext = v }

func (s *ParamContext) SetRvar(v antlr.Token) { s.rvar = v }

func (s *ParamContext) TEXT() antlr.TerminalNode {
	return s.GetToken(FSEParserTEXT, 0)
}

func (s *ParamContext) VAR() antlr.TerminalNode {
	return s.GetToken(FSEParserVAR, 0)
}

func (s *ParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ParamContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterParam(s)
	}
}

func (s *ParamContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitParam(s)
	}
}

func (p *FSEParser) Param() (localctx IParamContext) {
	localctx = NewParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, FSEParserRULE_param)
	p.EnterOuterAlt(localctx, 1)
	p.SetState(134)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case FSEParserTEXT:
		{
			p.SetState(132)

			var _m = p.Match(FSEParserTEXT)

			localctx.(*ParamContext).rtext = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case FSEParserVAR:
		{
			p.SetState(133)

			var _m = p.Match(FSEParserVAR)

			localctx.(*ParamContext).rvar = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIfgotoContext is an interface to support dynamic dispatch.
type IIfgotoContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetLvar returns the lvar token.
	GetLvar() antlr.Token

	// GetComp returns the comp token.
	GetComp() antlr.Token

	// GetGototarget returns the gototarget token.
	GetGototarget() antlr.Token

	// SetLvar sets the lvar token.
	SetLvar(antlr.Token)

	// SetComp sets the comp token.
	SetComp(antlr.Token)

	// SetGototarget sets the gototarget token.
	SetGototarget(antlr.Token)

	// Getter signatures
	AllVAR() []antlr.TerminalNode
	VAR(i int) antlr.TerminalNode
	NAME() antlr.TerminalNode
	Param() IParamContext

	// IsIfgotoContext differentiates from other interfaces.
	IsIfgotoContext()
}

type IfgotoContext struct {
	antlr.BaseParserRuleContext
	parser     antlr.Parser
	lvar       antlr.Token
	comp       antlr.Token
	gototarget antlr.Token
}

func NewEmptyIfgotoContext() *IfgotoContext {
	var p = new(IfgotoContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_ifgoto
	return p
}

func InitEmptyIfgotoContext(p *IfgotoContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_ifgoto
}

func (*IfgotoContext) IsIfgotoContext() {}

func NewIfgotoContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfgotoContext {
	var p = new(IfgotoContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_ifgoto

	return p
}

func (s *IfgotoContext) GetParser() antlr.Parser { return s.parser }

func (s *IfgotoContext) GetLvar() antlr.Token { return s.lvar }

func (s *IfgotoContext) GetComp() antlr.Token { return s.comp }

func (s *IfgotoContext) GetGototarget() antlr.Token { return s.gototarget }

func (s *IfgotoContext) SetLvar(v antlr.Token) { s.lvar = v }

func (s *IfgotoContext) SetComp(v antlr.Token) { s.comp = v }

func (s *IfgotoContext) SetGototarget(v antlr.Token) { s.gototarget = v }

func (s *IfgotoContext) AllVAR() []antlr.TerminalNode {
	return s.GetTokens(FSEParserVAR)
}

func (s *IfgotoContext) VAR(i int) antlr.TerminalNode {
	return s.GetToken(FSEParserVAR, i)
}

func (s *IfgotoContext) NAME() antlr.TerminalNode {
	return s.GetToken(FSEParserNAME, 0)
}

func (s *IfgotoContext) Param() IParamContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParamContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParamContext)
}

func (s *IfgotoContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfgotoContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfgotoContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterIfgoto(s)
	}
}

func (s *IfgotoContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitIfgoto(s)
	}
}

func (p *FSEParser) Ifgoto() (localctx IIfgotoContext) {
	localctx = NewIfgotoContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, FSEParserRULE_ifgoto)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(142)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == FSEParserT__15 {
		{
			p.SetState(136)
			p.Match(FSEParserT__15)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(137)

			var _m = p.Match(FSEParserVAR)

			localctx.(*IfgotoContext).lvar = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		p.SetState(140)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == FSEParserT__16 || _la == FSEParserT__17 {
			{
				p.SetState(138)

				var _lt = p.GetTokenStream().LT(1)

				localctx.(*IfgotoContext).comp = _lt

				_la = p.GetTokenStream().LA(1)

				if !(_la == FSEParserT__16 || _la == FSEParserT__17) {
					var _ri = p.GetErrorHandler().RecoverInline(p)

					localctx.(*IfgotoContext).comp = _ri
				} else {
					p.GetErrorHandler().ReportMatch(p)
					p.Consume()
				}
			}
			{
				p.SetState(139)
				p.Param()
			}

		}

	}
	p.SetState(147)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case FSEParserT__18:
		{
			p.SetState(144)
			p.Match(FSEParserT__18)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(145)

			var _lt = p.GetTokenStream().LT(1)

			localctx.(*IfgotoContext).gototarget = _lt

			_la = p.GetTokenStream().LA(1)

			if !(_la == FSEParserNAME || _la == FSEParserVAR) {
				var _ri = p.GetErrorHandler().RecoverInline(p)

				localctx.(*IfgotoContext).gototarget = _ri
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}

	case FSEParserT__19:
		{
			p.SetState(146)
			p.Match(FSEParserT__19)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITerminateContext is an interface to support dynamic dispatch.
type ITerminateContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsTerminateContext differentiates from other interfaces.
	IsTerminateContext()
}

type TerminateContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTerminateContext() *TerminateContext {
	var p = new(TerminateContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_terminate
	return p
}

func InitEmptyTerminateContext(p *TerminateContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = FSEParserRULE_terminate
}

func (*TerminateContext) IsTerminateContext() {}

func NewTerminateContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TerminateContext {
	var p = new(TerminateContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = FSEParserRULE_terminate

	return p
}

func (s *TerminateContext) GetParser() antlr.Parser { return s.parser }
func (s *TerminateContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TerminateContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TerminateContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.EnterTerminate(s)
	}
}

func (s *TerminateContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(FSEListener); ok {
		listenerT.ExitTerminate(s)
	}
}

func (p *FSEParser) Terminate() (localctx ITerminateContext) {
	localctx = NewTerminateContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, FSEParserRULE_terminate)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(149)
		p.Match(FSEParserT__20)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}
