package reg

import (
	"sort"

	"gitlab.com/lercher/wf/option"
	"go.temporal.io/sdk/workflow"
)

// Common is the combined key for an FSE workflow description.
// All other fields of Start are dependent of this two properties in terms
// of beeing registered with the Registry temporal workflow
type Common struct {
	WorkflowName string `json:"workflowName,omitempty"`
	Option       string `json:"option,omitempty"`
}

// DD are automatically comparable attributes of an FSE workflow description Start
type DD struct {
	Display     string `json:"display,omitempty"`
	Description string `json:"description,omitempty"`
	TaskQueue   string `json:"taskQueue,omitempty"`
}

// Start describes an FSE workflow
type Start struct {
	Common
	DD
	Roles []string `json:"roles,omitempty"`
	o     option.Option
}

// Equals is true, if all attributes match and the Roles slices are index-wise equal
func (s Start) Equals(s2 Start) bool {
	if s.Common != s2.Common || s.DD != s2.DD || len(s.Roles) != len(s2.Roles) {
		return false
	}
	for i := range s.Roles {
		if s.Roles[i] != s2.Roles[i] {
			return false
		}
	}
	return true
}

// IntersectsWith is true if e.Roles is empty or
// otherwise if there is a non-empty intersection between e.Roles
// and the given roles
func (s Start) IntersectsWith(roles []string) bool {
	if len(s.Roles) == 0 {
		// empty "for" list matches all roles
		return true
	}
	for _, r1 := range s.Roles {
		for _, r2 := range roles {
			if r1 == r2 {
				return true
			}
		}
	}
	return false
}

type regmap map[Common]Start

func filtered(m regmap, filter func(s Start) bool) []Start {
	var list []Start
	for _, st := range m {
		if filter(st) {
			list = append(list, st)
		}
	}
	sort.Slice(list, func(i, j int) bool {
		if list[i].Option == list[j].Option {
			return list[i].WorkflowName < list[j].WorkflowName
		}
		return list[i].Option < list[j].Option
	})
	return list
}

// Registry is a singleton workflow
func Registry(ctx workflow.Context) error {
	var err error
	logger := workflow.GetLogger(ctx)

	m := make(regmap)

	err = workflow.SetQueryHandler(ctx, QueryWorkflowWorkflows, func() ([]Start, error) {
		logger.Info("query all workflows")
		return filtered(m, func(s Start) bool { return true }), nil
	})
	if err != nil {
		return err
	}

	err = workflow.SetQueryHandler(ctx, QueryWorkflowWorkflowsFor, func(opt string, roles []string) ([]Start, error) {
		logger.Info("query workflows of type", "option", opt, "roles", roles)
		o, err := option.Parse(opt)
		if err != nil {
			return nil, err
		}
		return filtered(m, func(s Start) bool { return option.Match(s.o, o) && s.IntersectsWith(roles) }), nil
	})
	if err != nil {
		return err
	}

	err = workflow.SetQueryHandler(ctx, QueryWorkflowGetWorkflow, func(co Common) ([]Start, error) {
		logger.Info("get workflow", "co", co)
		return filtered(m, func(s Start) bool { return s.Common == co }), nil
	})
	if err != nil {
		return err
	}

	regChannel := workflow.GetSignalChannel(ctx, SignalWorkflowRegister)
	n := 0
	for {
		n++
		logger.Info("for-loop", "n", n)
		s := workflow.NewSelector(ctx)
		s.AddReceive(regChannel, func(c workflow.ReceiveChannel, more bool) {
			var start Start
			c.Receive(ctx, &start)
			logger.Info("register", "start", start)
			start.o, err = option.Parse(start.Option)
			if err != nil {
				logger.Error("option parse error", "option", start.Option, "err", err)
			}
			m[start.Common] = start
		})
		s.Select(ctx)
	}
}
