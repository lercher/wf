package reg

const (
	// QueryWorkflowWorkflows lists all known workflows (admin mode). It has no parameters.
	QueryWorkflowWorkflows = "workflows"
	// QueryWorkflowWorkflowsFor lists all workflows for the given option and roles. It has two parameters: string and []string.
	QueryWorkflowWorkflowsFor = "workflows-for"
	// QueryWorkflowGetWorkflow lists zero or one workflow of the given Common values. It has one parameter of type Comon.
	QueryWorkflowGetWorkflow = "get-workflow"
	// SignalWorkflowRegister is the signal's name to be passed to SignalWorkflow to register a new fse workflow type
	SignalWorkflowRegister = "register"
)

const (
	// TaskQueue is temporal's task queue name for hosting this Registry workflow
	TaskQueue = "_registry"
	// WorkflowID is this Registry workflow's ID as a singleton
	WorkflowID = "_global"
)
