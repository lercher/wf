package reg

import (
	"context"
	"errors"
	"fmt"

	"go.temporal.io/api/enums/v1"
	"go.temporal.io/sdk/client"
)

// Register queries for a Start type, if it doesn't exist or is different, replace it. This function also starts the Registry workflow if there is no instance of.
func Register(ctx context.Context, cli client.Client, s Start) error {
	existing, runID, err := workflows(ctx, cli)
	if err != nil {
		return err
	}
	fmt.Println("get got", existing)

	// if there is none or it differs, signal the new Start type
	if len(existing) == 0 || !existing[0].Equals(s) {
		err = cli.SignalWorkflow(ctx, WorkflowID, runID, SignalWorkflowRegister, s)
		if err != nil {
			return fmt.Errorf("signal wf start: %v", err)
		}
	}
	return nil
}

// WorkflowsOf returns all currently registered workflows filtered by option and a list of roles
func WorkflowsOf(ctx context.Context, cli client.Client, option string, roles []string) error {
	return errors.New("Not implemented")
}

// Workflows returns all currently registered workflows
func Workflows(ctx context.Context, cli client.Client) ([]Start, error) {
	list, _, err := workflows(ctx, cli)
	return list, err
}

func workflows(ctx context.Context, cli client.Client) ([]Start, string, error) {
	run, err := cli.ExecuteWorkflow(ctx, client.StartWorkflowOptions{ID: WorkflowID, TaskQueue: TaskQueue, WorkflowIDReusePolicy: enums.WORKFLOW_ID_REUSE_POLICY_ALLOW_DUPLICATE_FAILED_ONLY}, Registry)
	if err != nil {
		// it's no error, if there is a WorkflowIDReusePolicy violation
		return nil, "", fmt.Errorf("execute wf: %v", err)
	}
	fmt.Println("execute WF OK, runID", run.GetRunID())

	// try to get s from the registry WF
	runID := run.GetRunID()
	val, err := cli.QueryWorkflow(ctx, WorkflowID, runID, QueryWorkflowWorkflows)
	if err != nil {
		return nil, runID, fmt.Errorf("query wf: %v", err)
	}
	var existing []Start
	err = val.Get(&existing)
	if err != nil {
		return nil, runID, fmt.Errorf("query wf get value: %v", err)
	}

	return existing, run.GetRunID(), nil
}
