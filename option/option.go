package option

import (
	"fmt"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/lercher/wf/grammar/walker"
	"gitlab.com/lercher/wf/option/parser"
)

// Option is the result of Parse-ing an option string.
// An option string is of the form:
// `abc= value; y =value0 value1 ; ' z ' = '1st value for z' " 2nd value for z " 3rd; hidden;`
// i.e. a list of key=value value ...; or key; fragments, where key and value can be
// anything except "; = tab space cr lf" or anything quoted with single (') or double (") quoutes.
// If a key appears without a value in the list, it's single value is assumed to be "true".
// Whitespace is trimmed in the first form but not in the quoted form.
// There are no quotes of the same form allowed in the quoted form and there is no escaping.
// In the example above abc is [value], y is [value0 value1], " z " is the list of this three
// strings: "1st value for z" " 2nd value for z " "3rd", note the trimmed and not trimmed key/values.
// hidden is [true], so IsSet("hidden") returns true.
type Option map[string][]string

// Parse parses a new Option map
func Parse(s string) (Option, error) {
	input := antlr.NewInputStream(s)
	lexer := parser.NewOLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewOParser(stream)
	p.Interpreter.SetPredictionMode(antlr.PredictionModeSLL)

	// p has a default error listener that prints unwanted diagnostics, so we remove it first:
	el := &walker.ErrorListener{DiagnosticErrorListener: antlr.NewDiagnosticErrorListener(true)}
	p.RemoveErrorListeners()
	p.AddErrorListener(el)
	p.BuildParseTrees = true

	// now parse the FSE source reader
	tree := p.Main()
	if el.ErrorCount() > 0 {
		return nil, el
	}

	// create the AST by a walk of the parse tree
	w := newWlk(el, p.GetTokenStream())
	antlr.ParseTreeWalkerDefault.Walk(w, tree)
	if el.ErrorCount() > 0 {
		return nil, el
	}

	return w.option, nil
}

// GetValue looks up a particular option value.
// If doesn't exist or it has length zero an empty string is returned.
// If it has length one, the first element is returned.
// If it has length two or more, the slice is Sprint-ed and returned.
// If you need the splice, just use standard map lookup.
func (o Option) GetValue(k string) string {
	vals := o[k]
	switch len(vals) {
	case 0:
		return ""
	case 1:
		return vals[0]
	default:
		return fmt.Sprint(vals)
	}
}

// Match returns true, if for all keys that are common to o1 and o2
// their slices share at least one common element or both slices are empty
func Match(o1 Option, o2 Option) bool {
	switch {
	case len(o1) == 0 || len(o2) == 0:
		return true
	case len(o1) <= len(o2):
		for k, list1 := range o1 {
			if list2, ok := o2[k]; ok {
				if !listmatch(list1, list2) {
					return false
				}
			}
		}
		return true
	default:
		for k, list2 := range o2 {
			if list1, ok := o1[k]; ok {
				if !listmatch(list1, list2) {
					return false
				}
			}
		}
		return true
	}
}

func listmatch(l1, l2 []string) bool {
	switch {
	case len(l1) == 0 && len(l2) == 0:
		return true
	case len(l1) == 0 || len(l2) == 0:
		return false // can't have an element in common if only one list is non-empty
	default:
		for _, s1 := range l1 {
			for _, s2 := range l2 {
				if s1 == s2 {
					return true
				}
			}
		}
		return false
	}
}

// IsSet returns true if the list at k is exactly one
// element long and this single value is one of [true 1 on set yes]
func (o Option) IsSet(k string) bool {
	vals := o[k]
	switch len(vals) {
	default:
		return false
	case 1:
		v := vals[0]
		return v == "true" || v == "1" || v == "on" || v == "set" || v == "yes"
	}
}
