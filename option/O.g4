grammar O;

// Parsing option strings that can be embedded as a atring into the FSE grammar.
// It parses strings of the form "x=value; y=value0 value1; z='1st value for z' '2nd value for z' 3rd; hidden;"
// to a map[string][]string

WS: [ \t\r\n] -> channel(HIDDEN);

VALUE: 
      ( '\'' ~[']* '\'' ) 
    | ( '"'  ~["]* '"' ) 
    | ( ~[;=\t\r\n ]+ )
    ;

// handle characters which failed to match any other token, must be last
ERRORCHARACTER : . ;

// parser follows

main: 
    ( | keyvalue (';' keyvalue)* ';'? )
    EOF
;

keyvalue:
    name=VALUE 
    hasvalues?
;

hasvalues:
    '='
    singlevalue*
;

singlevalue:
    value=VALUE
;
