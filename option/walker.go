package option

import (
	"strings"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/lercher/wf/grammar/walker"
	"gitlab.com/lercher/wf/option/parser"
)

// wlk can be used to walk an antlr4 parse tree
type wlk struct {
	*parser.BaseOListener
	*walker.ErrorListener
	tokenstream antlr.TokenStream
	option      Option
	current     []string
}

// newWlk creates a new parse tree walker
func newWlk(el *walker.ErrorListener, ts antlr.TokenStream) *wlk {
	w := new(wlk)
	w.tokenstream = ts
	w.ErrorListener = el
	w.option = make(Option)
	return w
}

func (w *wlk) EnterKeyvalue(ctx *parser.KeyvalueContext) {
	w.current = []string{"true"}
}

func (w *wlk) EnterHasvalues(ctx *parser.HasvaluesContext) {
	w.current = nil
}

func (w *wlk) ExitKeyvalue(ctx *parser.KeyvalueContext) {
	name := value2string(ctx.GetName())
	w.option[name] = w.current
	w.current = nil
}

func (w *wlk) EnterSinglevalue(ctx *parser.SinglevalueContext) {
	tt := value2string(ctx.GetValue())
	w.current = append(w.current, tt)
}

func value2string(t antlr.Token) string {
	if t == nil {
		return ""
	}
	tt := t.GetText()
	switch {
	case len(tt) >= 2 && (strings.HasPrefix(tt, "'") && strings.HasSuffix(tt, "'") || strings.HasPrefix(tt, `"`) && strings.HasSuffix(tt, `"`)):
		tt = tt[1 : len(tt)-1]
	default:
		tt = strings.TrimSpace(tt)
	}
	return tt
}
