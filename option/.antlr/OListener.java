// Generated from c://git//src//gitlab.com//lercher//wf//option//O.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link OParser}.
 */
public interface OListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link OParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(OParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link OParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(OParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by {@link OParser#keyvalue}.
	 * @param ctx the parse tree
	 */
	void enterKeyvalue(OParser.KeyvalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link OParser#keyvalue}.
	 * @param ctx the parse tree
	 */
	void exitKeyvalue(OParser.KeyvalueContext ctx);
	/**
	 * Enter a parse tree produced by {@link OParser#hasvalues}.
	 * @param ctx the parse tree
	 */
	void enterHasvalues(OParser.HasvaluesContext ctx);
	/**
	 * Exit a parse tree produced by {@link OParser#hasvalues}.
	 * @param ctx the parse tree
	 */
	void exitHasvalues(OParser.HasvaluesContext ctx);
	/**
	 * Enter a parse tree produced by {@link OParser#singlevalue}.
	 * @param ctx the parse tree
	 */
	void enterSinglevalue(OParser.SinglevalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link OParser#singlevalue}.
	 * @param ctx the parse tree
	 */
	void exitSinglevalue(OParser.SinglevalueContext ctx);
}