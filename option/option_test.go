package option

import (
	"testing"

	"gitlab.com/lercher/wf/grammar/walker"
)

var o Option

func BenchmarkParseCplx(b *testing.B) {
	// BenchmarkParseCplx-12    	   43864	     27692 ns/op	   13544 B/op	     257 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse(" abc=something; y12 = value0\tvalue1 ;\nz-4 ='1st value for z'  \" 2nd 'value' for z \"  3rd ; ' ! '=!; hidden;")
		if err != nil {
			b.Error(err)
		}
	}
}

func BenchmarkParse3(b *testing.B) {
	// BenchmarkParse3-12    	   78998	     15604 ns/op	    8104 B/op	     162 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse("hidden=no; xyz = 1 2 3; hidden;")
		if err != nil {
			b.Error(err)
		}
	}
}

func BenchmarkParse1(b *testing.B) {
	// BenchmarkParse1-12    	  307692	      3935 ns/op	    2880 B/op	      56 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse("hidden")
		if err != nil {
			b.Error(err)
		}
	}
}

func TestParser(t *testing.T) {
	hiddenParser(t, "hidden")
	hiddenParser(t, "hidden=1")
	hiddenParser(t, "hidden=true")
	hiddenParser(t, "hidden=on")
	hiddenParser(t, "hidden=set")
	hiddenParser(t, "hidden=yes")

	hiddenParser(t, "hidden;")
	hiddenParser(t, "hidden=1;")
	hiddenParser(t, "hidden=true;")
	hiddenParser(t, "hidden =true;")
	hiddenParser(t, "hidden= true;")
	hiddenParser(t, "hidden = true;")
	hiddenParser(t, "hidden=on;")
	hiddenParser(t, "hidden=set;")
	hiddenParser(t, "hidden=yes;")

	hiddenParser(t, "hidden;hidden;")
	hiddenParser(t, "hidden=no;hidden;")
	hiddenParser(t, "hidden=no one ever;hidden;")
	hiddenParser(t, "hidden=no; xyz = 1 2 3; hidden;")
}

func hiddenParser(t *testing.T, have string) {
	t.Helper()
	o, err := Parse(have)
	if err != nil {
		el := err.(*walker.ErrorListener)
		for _, d := range el.Diagnostics {
			t.Error(d)
		}
		t.Errorf("have: %q", have)
		return
	}

	if got, want := o.IsSet("hidden"), true; got != want {
		t.Error(o)
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestParseValues(t *testing.T) {
	have := " abc=something; y12 = value0\tvalue1 ;\nz-4 ='1st value for z'  \" 2nd 'value' for z \"  3rd ; ' ! '=!; hidden;"
	o, err := Parse(have)
	if err != nil {
		el := err.(*walker.ErrorListener)
		for _, d := range el.Diagnostics {
			t.Error(d)
		}
		t.Fatal()
	}

	t.Log(o)

	if got, want := o.GetValue("abc"), "something"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("y12"), "[value0 value1]"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("z-4"), "[1st value for z  2nd 'value' for z  3rd]"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o["z-4"][2], "3rd"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue(" ! "), "!"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("unknown"), ""; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("unknown"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("hidden"), "true"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("hidden"), true; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("abc"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("y12"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestMatch(t *testing.T) {
	k1 := Option{"k": []string{"1"}}
	k2 := Option{"k": []string{"2"}}
	k12 := Option{"k": []string{"1", "2"}}

	l1 := Option{"l": []string{"1"}}
	l2 := Option{"l": []string{"2"}}
	l12 := Option{"l": []string{"1", "2"}}

	k12l12 := Option{"k": []string{"1", "2"}, "l": []string{"1", "2"}}
	k34l34 := Option{"k": []string{"3", "4"}, "l": []string{"3", "4"}}

	wantmatch(t, true, nil, nil)
	wantmatch(t, true, nil, k1)
	wantmatch(t, true, k1, nil)
	wantmatch(t, true, k1, k1)
	wantmatch(t, true, k12l12, k12l12)

	wantmatch(t, false, k1, k2)
	wantmatch(t, true, k12, k2)
	wantmatch(t, true, k1, l1)
	wantmatch(t, true, k12, l12)
	wantmatch(t, true, k2, l2)

	wantmatch(t, true, k1, k12l12)
	wantmatch(t, true, k12l12, k1)
	wantmatch(t, true, k2, k12l12)
	wantmatch(t, true, k12l12, k2)
	wantmatch(t, true, l12, k12l12)
	wantmatch(t, true, k12l12, l12)

	wantmatch(t, false, k12l12, k34l34)
	wantmatch(t, false, k34l34, k12l12)
	wantmatch(t, false, k12, k34l34)
	wantmatch(t, false, k34l34, k12)
	wantmatch(t, false, l12, k34l34)
	wantmatch(t, false, k34l34, l12)
	wantmatch(t, false, l1, k34l34)
	wantmatch(t, false, k34l34, l2)
}

func wantmatch(t *testing.T, ismatch bool, o1, o2 Option) {
	t.Helper()
	got := Match(o1, o2)
	if got != ismatch {
		t.Errorf("want match %v, got %v, have: %v --- %v", ismatch, got, o1, o2)
	}
}
