// Code generated from c://git//src//gitlab.com//lercher//wf//option//O.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // O

import "github.com/antlr4-go/antlr/v4"

// BaseOListener is a complete listener for a parse tree produced by OParser.
type BaseOListener struct{}

var _ OListener = &BaseOListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseOListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseOListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseOListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseOListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterMain is called when production main is entered.
func (s *BaseOListener) EnterMain(ctx *MainContext) {}

// ExitMain is called when production main is exited.
func (s *BaseOListener) ExitMain(ctx *MainContext) {}

// EnterKeyvalue is called when production keyvalue is entered.
func (s *BaseOListener) EnterKeyvalue(ctx *KeyvalueContext) {}

// ExitKeyvalue is called when production keyvalue is exited.
func (s *BaseOListener) ExitKeyvalue(ctx *KeyvalueContext) {}

// EnterHasvalues is called when production hasvalues is entered.
func (s *BaseOListener) EnterHasvalues(ctx *HasvaluesContext) {}

// ExitHasvalues is called when production hasvalues is exited.
func (s *BaseOListener) ExitHasvalues(ctx *HasvaluesContext) {}

// EnterSinglevalue is called when production singlevalue is entered.
func (s *BaseOListener) EnterSinglevalue(ctx *SinglevalueContext) {}

// ExitSinglevalue is called when production singlevalue is exited.
func (s *BaseOListener) ExitSinglevalue(ctx *SinglevalueContext) {}
