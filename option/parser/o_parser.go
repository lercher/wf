// Code generated from c://git//src//gitlab.com//lercher//wf//option//O.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // O

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type OParser struct {
	*antlr.BaseParser
}

var OParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func oParserInit() {
	staticData := &OParserStaticData
	staticData.LiteralNames = []string{
		"", "';'", "'='",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "WS", "VALUE", "ERRORCHARACTER",
	}
	staticData.RuleNames = []string{
		"main", "keyvalue", "hasvalues", "singlevalue",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 5, 38, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 1, 0, 1, 0,
		1, 0, 1, 0, 5, 0, 13, 8, 0, 10, 0, 12, 0, 16, 9, 0, 1, 0, 3, 0, 19, 8,
		0, 3, 0, 21, 8, 0, 1, 0, 1, 0, 1, 1, 1, 1, 3, 1, 27, 8, 1, 1, 2, 1, 2,
		5, 2, 31, 8, 2, 10, 2, 12, 2, 34, 9, 2, 1, 3, 1, 3, 1, 3, 0, 0, 4, 0, 2,
		4, 6, 0, 0, 38, 0, 20, 1, 0, 0, 0, 2, 24, 1, 0, 0, 0, 4, 28, 1, 0, 0, 0,
		6, 35, 1, 0, 0, 0, 8, 21, 1, 0, 0, 0, 9, 14, 3, 2, 1, 0, 10, 11, 5, 1,
		0, 0, 11, 13, 3, 2, 1, 0, 12, 10, 1, 0, 0, 0, 13, 16, 1, 0, 0, 0, 14, 12,
		1, 0, 0, 0, 14, 15, 1, 0, 0, 0, 15, 18, 1, 0, 0, 0, 16, 14, 1, 0, 0, 0,
		17, 19, 5, 1, 0, 0, 18, 17, 1, 0, 0, 0, 18, 19, 1, 0, 0, 0, 19, 21, 1,
		0, 0, 0, 20, 8, 1, 0, 0, 0, 20, 9, 1, 0, 0, 0, 21, 22, 1, 0, 0, 0, 22,
		23, 5, 0, 0, 1, 23, 1, 1, 0, 0, 0, 24, 26, 5, 4, 0, 0, 25, 27, 3, 4, 2,
		0, 26, 25, 1, 0, 0, 0, 26, 27, 1, 0, 0, 0, 27, 3, 1, 0, 0, 0, 28, 32, 5,
		2, 0, 0, 29, 31, 3, 6, 3, 0, 30, 29, 1, 0, 0, 0, 31, 34, 1, 0, 0, 0, 32,
		30, 1, 0, 0, 0, 32, 33, 1, 0, 0, 0, 33, 5, 1, 0, 0, 0, 34, 32, 1, 0, 0,
		0, 35, 36, 5, 4, 0, 0, 36, 7, 1, 0, 0, 0, 5, 14, 18, 20, 26, 32,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// OParserInit initializes any static state used to implement OParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewOParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func OParserInit() {
	staticData := &OParserStaticData
	staticData.once.Do(oParserInit)
}

// NewOParser produces a new parser instance for the optional input antlr.TokenStream.
func NewOParser(input antlr.TokenStream) *OParser {
	OParserInit()
	this := new(OParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &OParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "O.g4"

	return this
}

// OParser tokens.
const (
	OParserEOF            = antlr.TokenEOF
	OParserT__0           = 1
	OParserT__1           = 2
	OParserWS             = 3
	OParserVALUE          = 4
	OParserERRORCHARACTER = 5
)

// OParser rules.
const (
	OParserRULE_main        = 0
	OParserRULE_keyvalue    = 1
	OParserRULE_hasvalues   = 2
	OParserRULE_singlevalue = 3
)

// IMainContext is an interface to support dynamic dispatch.
type IMainContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	EOF() antlr.TerminalNode
	AllKeyvalue() []IKeyvalueContext
	Keyvalue(i int) IKeyvalueContext

	// IsMainContext differentiates from other interfaces.
	IsMainContext()
}

type MainContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMainContext() *MainContext {
	var p = new(MainContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_main
	return p
}

func InitEmptyMainContext(p *MainContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_main
}

func (*MainContext) IsMainContext() {}

func NewMainContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MainContext {
	var p = new(MainContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = OParserRULE_main

	return p
}

func (s *MainContext) GetParser() antlr.Parser { return s.parser }

func (s *MainContext) EOF() antlr.TerminalNode {
	return s.GetToken(OParserEOF, 0)
}

func (s *MainContext) AllKeyvalue() []IKeyvalueContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IKeyvalueContext); ok {
			len++
		}
	}

	tst := make([]IKeyvalueContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IKeyvalueContext); ok {
			tst[i] = t.(IKeyvalueContext)
			i++
		}
	}

	return tst
}

func (s *MainContext) Keyvalue(i int) IKeyvalueContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IKeyvalueContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IKeyvalueContext)
}

func (s *MainContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MainContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MainContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.EnterMain(s)
	}
}

func (s *MainContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.ExitMain(s)
	}
}

func (p *OParser) Main() (localctx IMainContext) {
	localctx = NewMainContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, OParserRULE_main)
	var _la int

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(20)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case OParserEOF:

	case OParserVALUE:
		{
			p.SetState(9)
			p.Keyvalue()
		}
		p.SetState(14)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 0, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
		for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
			if _alt == 1 {
				{
					p.SetState(10)
					p.Match(OParserT__0)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(11)
					p.Keyvalue()
				}

			}
			p.SetState(16)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 0, p.GetParserRuleContext())
			if p.HasError() {
				goto errorExit
			}
		}
		p.SetState(18)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == OParserT__0 {
			{
				p.SetState(17)
				p.Match(OParserT__0)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}
	{
		p.SetState(22)
		p.Match(OParserEOF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IKeyvalueContext is an interface to support dynamic dispatch.
type IKeyvalueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name token.
	GetName() antlr.Token

	// SetName sets the name token.
	SetName(antlr.Token)

	// Getter signatures
	VALUE() antlr.TerminalNode
	Hasvalues() IHasvaluesContext

	// IsKeyvalueContext differentiates from other interfaces.
	IsKeyvalueContext()
}

type KeyvalueContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	name   antlr.Token
}

func NewEmptyKeyvalueContext() *KeyvalueContext {
	var p = new(KeyvalueContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_keyvalue
	return p
}

func InitEmptyKeyvalueContext(p *KeyvalueContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_keyvalue
}

func (*KeyvalueContext) IsKeyvalueContext() {}

func NewKeyvalueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *KeyvalueContext {
	var p = new(KeyvalueContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = OParserRULE_keyvalue

	return p
}

func (s *KeyvalueContext) GetParser() antlr.Parser { return s.parser }

func (s *KeyvalueContext) GetName() antlr.Token { return s.name }

func (s *KeyvalueContext) SetName(v antlr.Token) { s.name = v }

func (s *KeyvalueContext) VALUE() antlr.TerminalNode {
	return s.GetToken(OParserVALUE, 0)
}

func (s *KeyvalueContext) Hasvalues() IHasvaluesContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IHasvaluesContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IHasvaluesContext)
}

func (s *KeyvalueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *KeyvalueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *KeyvalueContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.EnterKeyvalue(s)
	}
}

func (s *KeyvalueContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.ExitKeyvalue(s)
	}
}

func (p *OParser) Keyvalue() (localctx IKeyvalueContext) {
	localctx = NewKeyvalueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, OParserRULE_keyvalue)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(24)

		var _m = p.Match(OParserVALUE)

		localctx.(*KeyvalueContext).name = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(26)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == OParserT__1 {
		{
			p.SetState(25)
			p.Hasvalues()
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IHasvaluesContext is an interface to support dynamic dispatch.
type IHasvaluesContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllSinglevalue() []ISinglevalueContext
	Singlevalue(i int) ISinglevalueContext

	// IsHasvaluesContext differentiates from other interfaces.
	IsHasvaluesContext()
}

type HasvaluesContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyHasvaluesContext() *HasvaluesContext {
	var p = new(HasvaluesContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_hasvalues
	return p
}

func InitEmptyHasvaluesContext(p *HasvaluesContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_hasvalues
}

func (*HasvaluesContext) IsHasvaluesContext() {}

func NewHasvaluesContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *HasvaluesContext {
	var p = new(HasvaluesContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = OParserRULE_hasvalues

	return p
}

func (s *HasvaluesContext) GetParser() antlr.Parser { return s.parser }

func (s *HasvaluesContext) AllSinglevalue() []ISinglevalueContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(ISinglevalueContext); ok {
			len++
		}
	}

	tst := make([]ISinglevalueContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(ISinglevalueContext); ok {
			tst[i] = t.(ISinglevalueContext)
			i++
		}
	}

	return tst
}

func (s *HasvaluesContext) Singlevalue(i int) ISinglevalueContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISinglevalueContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISinglevalueContext)
}

func (s *HasvaluesContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *HasvaluesContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *HasvaluesContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.EnterHasvalues(s)
	}
}

func (s *HasvaluesContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.ExitHasvalues(s)
	}
}

func (p *OParser) Hasvalues() (localctx IHasvaluesContext) {
	localctx = NewHasvaluesContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, OParserRULE_hasvalues)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(28)
		p.Match(OParserT__1)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(32)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == OParserVALUE {
		{
			p.SetState(29)
			p.Singlevalue()
		}

		p.SetState(34)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISinglevalueContext is an interface to support dynamic dispatch.
type ISinglevalueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetValue returns the value token.
	GetValue() antlr.Token

	// SetValue sets the value token.
	SetValue(antlr.Token)

	// Getter signatures
	VALUE() antlr.TerminalNode

	// IsSinglevalueContext differentiates from other interfaces.
	IsSinglevalueContext()
}

type SinglevalueContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	value  antlr.Token
}

func NewEmptySinglevalueContext() *SinglevalueContext {
	var p = new(SinglevalueContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_singlevalue
	return p
}

func InitEmptySinglevalueContext(p *SinglevalueContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = OParserRULE_singlevalue
}

func (*SinglevalueContext) IsSinglevalueContext() {}

func NewSinglevalueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SinglevalueContext {
	var p = new(SinglevalueContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = OParserRULE_singlevalue

	return p
}

func (s *SinglevalueContext) GetParser() antlr.Parser { return s.parser }

func (s *SinglevalueContext) GetValue() antlr.Token { return s.value }

func (s *SinglevalueContext) SetValue(v antlr.Token) { s.value = v }

func (s *SinglevalueContext) VALUE() antlr.TerminalNode {
	return s.GetToken(OParserVALUE, 0)
}

func (s *SinglevalueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SinglevalueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SinglevalueContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.EnterSinglevalue(s)
	}
}

func (s *SinglevalueContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(OListener); ok {
		listenerT.ExitSinglevalue(s)
	}
}

func (p *OParser) Singlevalue() (localctx ISinglevalueContext) {
	localctx = NewSinglevalueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, OParserRULE_singlevalue)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(35)

		var _m = p.Match(OParserVALUE)

		localctx.(*SinglevalueContext).value = _m
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}
