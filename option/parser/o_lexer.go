// Code generated from c://git//src//gitlab.com//lercher//wf//option//O.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser

import (
	"fmt"
	"github.com/antlr4-go/antlr/v4"
	"sync"
	"unicode"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = sync.Once{}
var _ = unicode.IsLetter

type OLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var OLexerLexerStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	ChannelNames           []string
	ModeNames              []string
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func olexerLexerInit() {
	staticData := &OLexerLexerStaticData
	staticData.ChannelNames = []string{
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	}
	staticData.ModeNames = []string{
		"DEFAULT_MODE",
	}
	staticData.LiteralNames = []string{
		"", "';'", "'='",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "WS", "VALUE", "ERRORCHARACTER",
	}
	staticData.RuleNames = []string{
		"T__0", "T__1", "WS", "VALUE", "ERRORCHARACTER",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 0, 5, 44, 6, -1, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2,
		4, 7, 4, 1, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 3, 1, 3, 5,
		3, 22, 8, 3, 10, 3, 12, 3, 25, 9, 3, 1, 3, 1, 3, 1, 3, 5, 3, 30, 8, 3,
		10, 3, 12, 3, 33, 9, 3, 1, 3, 1, 3, 4, 3, 37, 8, 3, 11, 3, 12, 3, 38, 3,
		3, 41, 8, 3, 1, 4, 1, 4, 0, 0, 5, 1, 1, 3, 2, 5, 3, 7, 4, 9, 5, 1, 0, 4,
		3, 0, 9, 10, 13, 13, 32, 32, 1, 0, 39, 39, 1, 0, 34, 34, 5, 0, 9, 10, 13,
		13, 32, 32, 59, 59, 61, 61, 48, 0, 1, 1, 0, 0, 0, 0, 3, 1, 0, 0, 0, 0,
		5, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 1, 11, 1, 0, 0, 0, 3,
		13, 1, 0, 0, 0, 5, 15, 1, 0, 0, 0, 7, 40, 1, 0, 0, 0, 9, 42, 1, 0, 0, 0,
		11, 12, 5, 59, 0, 0, 12, 2, 1, 0, 0, 0, 13, 14, 5, 61, 0, 0, 14, 4, 1,
		0, 0, 0, 15, 16, 7, 0, 0, 0, 16, 17, 1, 0, 0, 0, 17, 18, 6, 2, 0, 0, 18,
		6, 1, 0, 0, 0, 19, 23, 5, 39, 0, 0, 20, 22, 8, 1, 0, 0, 21, 20, 1, 0, 0,
		0, 22, 25, 1, 0, 0, 0, 23, 21, 1, 0, 0, 0, 23, 24, 1, 0, 0, 0, 24, 26,
		1, 0, 0, 0, 25, 23, 1, 0, 0, 0, 26, 41, 5, 39, 0, 0, 27, 31, 5, 34, 0,
		0, 28, 30, 8, 2, 0, 0, 29, 28, 1, 0, 0, 0, 30, 33, 1, 0, 0, 0, 31, 29,
		1, 0, 0, 0, 31, 32, 1, 0, 0, 0, 32, 34, 1, 0, 0, 0, 33, 31, 1, 0, 0, 0,
		34, 41, 5, 34, 0, 0, 35, 37, 8, 3, 0, 0, 36, 35, 1, 0, 0, 0, 37, 38, 1,
		0, 0, 0, 38, 36, 1, 0, 0, 0, 38, 39, 1, 0, 0, 0, 39, 41, 1, 0, 0, 0, 40,
		19, 1, 0, 0, 0, 40, 27, 1, 0, 0, 0, 40, 36, 1, 0, 0, 0, 41, 8, 1, 0, 0,
		0, 42, 43, 9, 0, 0, 0, 43, 10, 1, 0, 0, 0, 5, 0, 23, 31, 38, 40, 1, 0,
		1, 0,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// OLexerInit initializes any static state used to implement OLexer. By default the
// static state used to implement the lexer is lazily initialized during the first call to
// NewOLexer(). You can call this function if you wish to initialize the static state ahead
// of time.
func OLexerInit() {
	staticData := &OLexerLexerStaticData
	staticData.once.Do(olexerLexerInit)
}

// NewOLexer produces a new lexer instance for the optional input antlr.CharStream.
func NewOLexer(input antlr.CharStream) *OLexer {
	OLexerInit()
	l := new(OLexer)
	l.BaseLexer = antlr.NewBaseLexer(input)
	staticData := &OLexerLexerStaticData
	l.Interpreter = antlr.NewLexerATNSimulator(l, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	l.channelNames = staticData.ChannelNames
	l.modeNames = staticData.ModeNames
	l.RuleNames = staticData.RuleNames
	l.LiteralNames = staticData.LiteralNames
	l.SymbolicNames = staticData.SymbolicNames
	l.GrammarFileName = "O.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// OLexer tokens.
const (
	OLexerT__0           = 1
	OLexerT__1           = 2
	OLexerWS             = 3
	OLexerVALUE          = 4
	OLexerERRORCHARACTER = 5
)
