// Code generated from c://git//src//gitlab.com//lercher//wf//option//O.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser // O

import "github.com/antlr4-go/antlr/v4"

// OListener is a complete listener for a parse tree produced by OParser.
type OListener interface {
	antlr.ParseTreeListener

	// EnterMain is called when entering the main production.
	EnterMain(c *MainContext)

	// EnterKeyvalue is called when entering the keyvalue production.
	EnterKeyvalue(c *KeyvalueContext)

	// EnterHasvalues is called when entering the hasvalues production.
	EnterHasvalues(c *HasvaluesContext)

	// EnterSinglevalue is called when entering the singlevalue production.
	EnterSinglevalue(c *SinglevalueContext)

	// ExitMain is called when exiting the main production.
	ExitMain(c *MainContext)

	// ExitKeyvalue is called when exiting the keyvalue production.
	ExitKeyvalue(c *KeyvalueContext)

	// ExitHasvalues is called when exiting the hasvalues production.
	ExitHasvalues(c *HasvaluesContext)

	// ExitSinglevalue is called when exiting the singlevalue production.
	ExitSinglevalue(c *SinglevalueContext)
}
