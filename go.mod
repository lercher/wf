module gitlab.com/lercher/wf

go 1.14

require (
	github.com/antlr4-go/antlr/v4 v4.13.0
	github.com/yuin/goldmark v1.6.0
	go.temporal.io/api v0.28.0
	go.temporal.io/sdk v0.28.0
)
