# Using temporal.io with podman

This is quite a straight-forward conversion of the provided
[docker-compose.yml](docker-compose.yml) file to
a k8s pod specification to be used via `podman play`.

The only important difference is, that the pod internal
communication goes over localhost and not a
docker internal DNS name. So we need to specify that
the temporal server binds to 127.0.0.1 instead of the
host's proper IP address, which is the default.

## Creating and Starting the services as a single pod

```sh
podman play kube temporal.yaml
```

## Starting/Stopping the pod

```sh
podman pod start/stop temporal-27
```

## Storing Cassandra Data locally

The cassandra container stores its data files in
`/var/lib/cassandra`, so interactively you would
give podman the option

```sh
$ podman ... -v /my/own/datadir:/var/lib/cassandra ...
```

However, I guess, we should to do this in the k8s yaml file. But
I can't find docs on that one, only issues. So we probably
stick with storing data within the cassandra container.

It's a different story with prod deployments, of course. But
there we're going to run persistent storage on a different
server anyway.

## Inspecting the Server

```sh
podman exec -it temporal-srv sh
```

E.g. listing open TCP ports:

```txt
etc/temporal/config # netstat -tulpn
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:9042            0.0.0.0:*               LISTEN      -
tcp        0      0 :::8088                 :::*                    LISTEN      -
tcp        0      0 10.0.2.100:7233         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:7234         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:7235         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:7239         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:6933         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:6934         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:6935         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:6939         0.0.0.0:*               LISTEN      117/temporal-server
tcp        0      0 10.0.2.100:7000         0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:8081          0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:46653         0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:7199          0.0.0.0:*               LISTEN      -
/etc/temporal/config #
```

## Other Directories

Other directories contain a draft for a finite state machine
DSL, a workflow type registry plus an interpreter
running on temporal. However, this is currently in
and stays in experimental state. It is planned to have a
stable, MIT licensed version of the DSL, the registry and the
interpreter under gitlab.com/lercher/fse-temporal.
