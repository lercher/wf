package main

// go run . -temporal linux-home:7233

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/lercher/wf/reg"

	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

var (
	flagHostPort = flag.String("temporal", "", "host:port of a temporal.io instance, the standard port is 7233. Use dns:/// prefix to enable DNS based round-robin.")
)

func main() {
	flag.Parse()

	// The client is a heavyweight object that should be created once
	serviceClient, err := client.NewClient(client.Options{
		HostPort: *flagHostPort,
	})
	if err != nil {
		log.Fatalf("new temporal client: %v", err)
	}
	defer serviceClient.Close()

	worker := worker.New(serviceClient, reg.TaskQueue, worker.Options{})

	worker.RegisterWorkflow(reg.Registry)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		log.Println("Stoping")
		worker.Stop()
		os.Exit(0)
	}()

	err = worker.Start()
	if err != nil {
		log.Fatalf("Unable to start worker: %v", err)
	}
	select {} // go idle here
}
