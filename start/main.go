package main

// go run . -temporal linux-home:7233

import (
	"context"
	"flag"
	"log"
	"time"

	"gitlab.com/lercher/wf/reg"

	"go.temporal.io/sdk/client"
)

var (
	flagHostPort = flag.String("temporal", "", "host:port of a temporal.io instance, the standard port is 7233. Use dns:/// prefix to enable DNS based round-robin.")
)

func main() {
	flag.Parse()

	// The client is a heavyweight object that should be created once
	serviceClient, err := client.NewClient(client.Options{
		HostPort: *flagHostPort,
	})
	if err != nil {
		log.Fatalf("new temporal client: %v", err)
	}
	defer serviceClient.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = reg.Register(ctx, serviceClient, reg.Start{
		Common: reg.Common{
			WorkflowName: "VTVertragBeendigung",
			Option:       "VTVertrag",
		},
		DD: reg.DD{
			TaskQueue:   "nonExisting_tq_",
			Display:     "Vertragsbeendigung",
			Description: "Starten Sie diesen Workflow, um einen Vertragsbeendigungsprozess zu beginnen",
		},
	})
	if err != nil {
		log.Fatalf("Unable to register: %v", err)
	}
}
