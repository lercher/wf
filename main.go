package main

import (
	"flag"
	"log"

	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

var (
	flagHostPort = flag.String("temporal", "", "host:port of a temporal.io instance, the standard port is 7233. Use dns:/// prefix to enable DNS based round-robin.")
)

func main() {
	flag.Parse()

	// The client is a heavyweight object that should be created once
	serviceClient, err := client.NewClient(client.Options{
		HostPort: *flagHostPort,
	})

	if err != nil {
		log.Fatalf("new temporal client: %v", err)
	}

	worker := worker.New(serviceClient, "tutorial_tq", worker.Options{})

	worker.RegisterWorkflow(Greetings)
	worker.RegisterActivity(GetUser)
	worker.RegisterActivity(SendGreeting)

	err = worker.Start()
	if err != nil {
		log.Fatalf("Unable to start worker: %v", err)
	}

	select {}
}
