package main

// go run . -temporal linux-home:7233 -f simple.fse
// go run . -temporal tumble:7233     -f simple.fse

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"gitlab.com/lercher/wf/grammar/exec"
	"gitlab.com/lercher/wf/grammar/walker"
	"gitlab.com/lercher/wf/reg"

	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
	"go.temporal.io/sdk/workflow"
)

var (
	flagHostPort  = flag.String("temporal", "tumble:7233", "host:port of a temporal.io instance, the standard port is 7233. Use dns:/// prefix to enable DNS based round-robin.")
	flagFSE       = flag.String("f", "*.fse", "host all temporal workflows defined by this glob pattern.")
	flagTaskQueue = flag.String("q", "fse", "Task queue name for this' process temporal workflows and activities, i.e. where clients post tasks to to reach this process.")
	flagActionTQ  = flag.String("aq", "", "Activities task queue name for calling externally implemented activities. If empty, adds '-actions' to this process' task queue name.")
	flagPort      = flag.Int("p", 9001, "host a web GUI on http://localhost:this-port/ if `port` is greater than 0")
)

func main() {
	flag.Parse()

	aq := *flagActionTQ
	if aq == "" {
		aq = *flagTaskQueue + "-actions"
	}

	wfs, err := globWorkflows(*flagFSE)
	if err != nil {
		log.Fatalln(err)
	}

	// The client is a heavyweight object that should be created once
	serviceClient, err := client.NewClient(client.Options{
		HostPort: *flagHostPort,
	})
	if err != nil {
		log.Fatalln("service client:", err)
	}

	registryWorker := worker.New(serviceClient, reg.TaskQueue, worker.Options{})
	registryWorker.RegisterWorkflow(reg.Registry)
	err = registryWorker.Start()
	if err != nil {
		log.Fatalln("registry worker:", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	taskQueue := *flagTaskQueue
	fseWorker := worker.New(serviceClient, taskQueue, worker.Options{})
	for _, wf := range wfs {
		fseWorker.RegisterWorkflowWithOptions(makeWorkflow(wf, aq), workflow.RegisterOptions{
			Name: wf.Name,
		})

		err = reg.Register(ctx, serviceClient, wf2start(wf, taskQueue))
		if err != nil {
			log.Fatalln("register:", err)
		}
	}
	err = fseWorker.Start()
	if err != nil {
		log.Fatalln("fse worker:", err)
	}

	var p = fmt.Sprintf(":%d", *flagPort)
	log.Println()
	log.Println("Have a look at", "http://"+strings.Split(*flagHostPort, ":")[0]+":8088/", "for the temporal admin UI")
	if *flagPort > 0 {
		log.Println("Have a look at", fmt.Sprintf("http://localhost%s/", p), "for interactively working with fse workflows")
	}
	log.Println()

	var srv *http.Server
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		log.Println("Stoping workers ...")
		fseWorker.Stop()
		registryWorker.Stop()
		if srv != nil {
			log.Println("Stoping http server ...")
			srv.Shutdown(context.Background())
		}
		log.Println("Done.")
		os.Exit(0)
	}()

	if *flagPort == 0 {
		select {} // go idle here
	} else {
		mux := setupHandlers(serviceClient)
		srv = &http.Server{Addr: p, Handler: mux}

		// start web server
		// always returns error. ErrServerClosed on graceful close
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			// unexpected error. port in use?
			log.Fatalf("ListenAndServe: %v", err)
		}
	}
}

func wf2start(wf *walker.Workflow, taskQueue string) reg.Start {
	return reg.Start{
		Common: reg.Common{
			WorkflowName: wf.Name,
			Option:       wf.Option,
		},
		DD: reg.DD{
			Display:     wf.Display,
			Description: wf.Description,
			TaskQueue:   taskQueue,
		},
		Roles: wf.Roles,
	}
}

func globWorkflows(pattern string) ([]*walker.Workflow, error) {
	m, err0 := filepath.Glob(pattern)
	if err0 != nil {
		return nil, err0
	}
	var err error
	list := make([]*walker.Workflow, 0, len(m))
	for _, fn := range m {
		wf, err1 := loadWF(fn)
		if err1 != nil {
			err = err1
			continue
		}
		list = append(list, wf)
	}
	return list, err
}

func loadWF(fn string) (*walker.Workflow, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	wf, diag, err := walker.Parse(f)
	if err != nil {
		for i := range diag {
			log.Println(fn, diag[i].String())
		}
		return nil, fmt.Errorf("%s: %v", fn, err)
	}

	log.Println("wf loaded successfully from", fn, "-",
		"name:", wf.Name,
		"display:", wf.Display,
		"states:", len(wf.AllStates),
		"toplevelStates:", len(wf.States),
		"description:", wf.Description,
	)

	return wf, nil
}

func makeWorkflow(wf *walker.Workflow, actionsTaskQueue string) func(ctx workflow.Context) error {
	return func(ctx workflow.Context) error {
		in := exec.New(ctx, wf, nil)
		in.ActivityOptions.TaskQueue = actionsTaskQueue
		return in.Run()
	}
}
