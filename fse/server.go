package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/lercher/wf/grammar/exec"
	"gitlab.com/lercher/wf/reg"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"

	commonV1 "go.temporal.io/api/common/v1"
	workflowV1 "go.temporal.io/api/workflow/v1"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/converter"
)

type server struct {
	client.Client
}

// ListInfo is passed to the list.html template
type ListInfo struct {
	Startables    []reg.Start
	OpenWorkflows []*workflowV1.WorkflowExecutionInfo
}

// ItemInfo is passed to the item.html template
type ItemInfo struct {
	exec.WFState
}

// CreateInfo is passed to the create.html template
type CreateInfo struct {
	WorkflowName string
	client.StartWorkflowOptions
	client.WorkflowRun
}

var funcMap = template.FuncMap{
	"dt": func(t time.Time) string {
		return t.Local().Format("02.01.2006 15:04:05")
	},
	"payload": func(v *commonV1.Payload) (interface{}, error) {
		var val interface{}
		err := converter.GetDefaultDataConverter().FromPayload(v, &val)
		return val, err
	},
	"markdown": func(s string) (template.HTML, error) {
		md := goldmark.New(
			goldmark.WithExtensions(extension.GFM),
		)
		var buf bytes.Buffer
		if err := md.Convert([]byte(s), &buf); err != nil {
			return "", err
		}
		return template.HTML(buf.String()), nil
	},
}

func setupHandlers(serviceClient client.Client) *http.ServeMux {
	s := server{serviceClient}

	mux := http.NewServeMux()
	mux.HandleFunc("/create", s.create)
	mux.HandleFunc("/item", s.item)
	mux.HandleFunc("/trigger", s.trigger)
	mux.HandleFunc("/terminate", s.terminate)
	mux.HandleFunc("/", s.root)
	return mux
}

func (s server) template(w http.ResponseWriter, r *http.Request, fn string, data interface{}) error {
	t, err := template.New("").Funcs(funcMap).ParseFiles(fn)
	if err != nil {
		return err
	}
	return t.ExecuteTemplate(w, fn, data)
}

func (s server) root(w http.ResponseWriter, r *http.Request) {
	st, err := reg.Workflows(r.Context(), s.Client)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ow, err := listExecutions(r.Context(), s.Client, "")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.template(w, r, "root.html", ListInfo{st, ow})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s server) item(w http.ResponseWriter, r *http.Request) {
	WorkflowID := r.URL.Query().Get("id")
	RunID := ""

	// val, err := s.Client.QueryWorkflow(r.Context(), WorkflowID, RunID, exec.QueryWorkflowWFStateFor, nil) // send roles instead of nil
	val, err := s.Client.QueryWorkflow(r.Context(), WorkflowID, RunID, exec.QueryWorkflowWFState) // admin view of wfstate
	if err != nil {
		err = fmt.Errorf("query wf: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var state exec.WFState
	if !val.HasValue() {
		err = fmt.Errorf("query wf get value: has no value")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = val.Get(&state)
	if err != nil {
		err = fmt.Errorf("query wf get value: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println(state)

	err = s.template(w, r, "item.html", ItemInfo{state})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s server) trigger(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	WorkflowID := r.FormValue("id")
	RunID := ""
	Trigger := r.FormValue("trigger")

	err = s.Client.SignalWorkflow(r.Context(), WorkflowID, RunID, exec.SignalWorkflowSignalEvent, Trigger)
	if err != nil {
		err = fmt.Errorf("signal %q: %v", Trigger, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redir := fmt.Sprintf("/item?id=%s", url.QueryEscape(WorkflowID))
	http.Redirect(w, r, redir, http.StatusTemporaryRedirect)
}

func (s server) terminate(w http.ResponseWriter, r *http.Request) {
	WorkflowID := r.FormValue("id")
	RunID := r.FormValue("runid")

	err := s.Client.TerminateWorkflow(r.Context(), WorkflowID, RunID, "terminated by user request")
	if err != nil {
		err = fmt.Errorf("terminate %v/%v: %v", WorkflowID, RunID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redir := fmt.Sprintf("/item?id=%s", url.QueryEscape(WorkflowID))
	if WorkflowID == reg.WorkflowID {
		redir = "/"
	}
	http.Redirect(w, r, redir, http.StatusTemporaryRedirect)
}

func (s server) create(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	WorkflowName := r.FormValue("WorkflowName")
	Option := r.FormValue("Option")
	ID := r.FormValue("ID")
	User := r.FormValue("User")
	TaskQueue := r.FormValue("TaskQueue")

	wfid := fmt.Sprintf("%s.%s.%s", WorkflowName, Option, ID)

	wfo := client.StartWorkflowOptions{
		ID:        wfid,
		TaskQueue: TaskQueue,
		Memo: map[string]interface{}{
			"username": User,
		},
	}
	we, err := s.Client.ExecuteWorkflow(r.Context(), wfo, WorkflowName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.template(w, r, "create.html", CreateInfo{WorkflowName, wfo, we})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
